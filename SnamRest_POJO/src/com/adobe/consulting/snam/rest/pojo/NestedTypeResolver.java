package com.adobe.consulting.snam.rest.pojo;

import java.util.Collection;

import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.jsontype.NamedType;
import com.fasterxml.jackson.databind.jsontype.TypeDeserializer;
import com.fasterxml.jackson.databind.jsontype.TypeIdResolver;
import com.fasterxml.jackson.databind.jsontype.impl.StdTypeResolverBuilder;

public class NestedTypeResolver extends StdTypeResolverBuilder {
    @Override
    public TypeDeserializer buildTypeDeserializer(DeserializationConfig config, JavaType baseType,
            Collection<NamedType> subtypes) {
            //Copied this code from parent class, StdTypeResolverBuilder with same method name
            TypeIdResolver idRes = idResolver(config, baseType, subtypes, false, true);
            return new NestedTypeDeserializer(baseType, idRes, _typeProperty, _typeIdVisible,
                null, _includeAs);
    }
}