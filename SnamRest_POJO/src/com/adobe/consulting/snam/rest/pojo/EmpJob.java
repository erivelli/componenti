package com.adobe.consulting.snam.rest.pojo;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EmpJob implements SFOData, Serializable {

	@JsonProperty("__metadata")
	private GenericMetadata metadata;

	@JsonProperty("userId")
	private String userId;
	
	@JsonProperty("personIdExternal")
	private String personIdExternal;

	@JsonProperty("formalName")
	private String formalName;

	@JsonProperty("startDate")
	private String startDate;
	
	@JsonProperty("endDate")
	private String endDate;

	@JsonProperty("payGrade")
	private String payGrade;

	@JsonProperty("company")
	private String company;

	@JsonProperty("eventReason")
	private String eventReason;

	@JsonProperty("payScaleLevel")
	private String payScaleLevel;
	
	//NAVIGATIONS
	@JsonProperty("employmentNav")
	EmpEmployment employmentNav = null;
	
	@JsonProperty("payScaleLevelNav")
	PayScaleLevel payScaleLevelNav = null;

	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getStartDate() {
		if(startDate != null && !startDate.equals("")) {
			if(startDate.contains("/Date")) {
				SimpleDateFormat dateFormat = new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				String filteredDate = startDate.replace("/Date(", "").replace(")/", "");

				long timestamp = Long.parseLong(filteredDate);
				Date newDate = new Date(timestamp);
				return dateFormat.format(newDate);
			}
		}
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		if(endDate != null && !endDate.equals("")) {
			if(endDate.contains("/Date")) {
				SimpleDateFormat dateFormat = new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				String filteredDate = endDate.replace("/Date(", "").replace(")/", "");

				long timestamp = Long.parseLong(filteredDate);
				Date newDate = new Date(timestamp);
				return dateFormat.format(newDate);
			}
		}
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getPayGrade() {
		return payGrade;
	}
	public void setPayGrade(String payGrade) {
		this.payGrade = payGrade;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getEventReason() {
		return eventReason;
	}
	public void setEventReason(String eventReason) {
		this.eventReason = eventReason;
	}
	public String getPayScaleLevel() {
		return payScaleLevel;
	}
	public void setPayScaleLevel(String payScaleLevel) {
		this.payScaleLevel = payScaleLevel;
	}
	@Override
	public GenericMetadata getMetadata() {
		return this.metadata;
	}
	@Override
	public void setMetadata(GenericMetadata metadata) {
		this.metadata = metadata;
	}
	@Override
	public String getPersonIdExternal() {
		return personIdExternal;
	}
	@Override
	public void setPersonIdExternal(String personIdExternal) {
		this.personIdExternal = personIdExternal;
	}
	@Override
	public String getFormalName() {
		return formalName;
	}
	@Override
	public void setFormalName(String formalName) {
		this.formalName = formalName;
	}

	public EmpEmployment getEmploymentNav() {
		return employmentNav;
	}
	public void setEmploymentNav(EmpEmployment employmentNav) {
		this.employmentNav = employmentNav;
	}
	
	public PayScaleLevel getPayScaleLevelNav() {
		return payScaleLevelNav;
	}
	
	public void setPayScaleLevelNav(PayScaleLevel payScaleLevelNav) {
		this.payScaleLevelNav = payScaleLevelNav;
	}
}
