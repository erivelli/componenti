package com.adobe.consulting.snam.rest.pojo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ServiceResult implements Serializable {
	
	@JsonProperty("d")
	ServiceResponse response;
	
	@JsonProperty("error")
	ServiceResponseError error;
	
	public void setResponse(ServiceResponse response) {
		this.response = response;
	}
	
	public ServiceResponse getResponse() {
		return response;
	}
	
	public void setError(ServiceResponseError responseError) {
		this.error = responseError;
	}
	
	public ServiceResponseError getError() {
		return error;
	}
	
}
