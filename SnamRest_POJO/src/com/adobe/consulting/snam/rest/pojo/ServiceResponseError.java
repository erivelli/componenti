package com.adobe.consulting.snam.rest.pojo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ServiceResponseError implements Serializable {

	@JsonProperty("code")
	protected String code;

	@JsonProperty("message")
	protected ServiceResponseErrorMessage message;
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public ServiceResponseErrorMessage getMessage() {
		return message;
	}

	public void setMessage(ServiceResponseErrorMessage message) {
		this.message = message;
	}

}
