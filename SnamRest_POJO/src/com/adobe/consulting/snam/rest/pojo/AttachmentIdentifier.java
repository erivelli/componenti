package com.adobe.consulting.snam.rest.pojo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AttachmentIdentifier implements Serializable{
	
	@JsonProperty("__metadata")
	private AttachmentMetadata __metadata = new AttachmentMetadata();
	
	public AttachmentMetadata getMetadata() {
		return __metadata;
	}

	public void setMetadata(AttachmentMetadata metadata) {
		this.__metadata = metadata;
	}
}
