package com.adobe.consulting.snam.rest.pojo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ServiceResponseErrorMessage implements Serializable{
	
	@JsonProperty("lang")
	String lang;
	
	@JsonProperty("value")
	String value;
	
	
	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
}
