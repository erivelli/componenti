package com.adobe.consulting.snam.rest.pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonTypeResolver;

public class UpsertResponse implements Serializable
{
	
	@JsonProperty("key")
	protected String key;
	
	@JsonProperty("status")
	protected String status;
	
	@JsonProperty("editStatus")
	protected String editStatus;
	
	@JsonProperty("message")
	protected String message;
	
	@JsonProperty("index")
	protected int index;
	
	@JsonProperty("httpCode")
	protected String httpCode;
	
	@JsonProperty("inlineResults")
	protected String inlineResults;
	
	
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getEditStatus() {
		return editStatus;
	}

	public void setEditStatus(String editStatus) {
		this.editStatus = editStatus;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String getHttpCode() {
		return httpCode;
	}

	public void setHttpCode(String httpCode) {
		this.httpCode = httpCode;
	}

	public String getInlineResults() {
		return inlineResults;
	}

	public void setInlineResults(String inlineResults) {
		this.inlineResults = inlineResults;
	}
	
}
