package com.adobe.consulting.snam.rest.pojo;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PerPersonal implements SFOData, Serializable {

	@JsonProperty("__metadata")
	GenericMetadata metadata = null;

	@JsonProperty("personIdExternal")
	private String personIdExternal;

	@JsonProperty("lastName")
	private String lastName;
	
	@JsonProperty("formalName")
	private String formalName;
	
	@JsonProperty("startDate")
	private String startDate;
	
	@JsonProperty("lastModifiedDateTime")
	private String lastModifiedDateTime;
	
	public GenericMetadata getMetadata() {
		return metadata;
	}
	public void setMetadata(GenericMetadata metadata) {
		this.metadata = metadata;
	}
	public String getPersonIdExternal() {
		return personIdExternal;
	}
	public void setPersonIdExternal(String personIdExternal) {
		this.personIdExternal = personIdExternal;
	}
	public String getFormalName() {
		return formalName;
	}
	public void setFormalName(String formalName) {
		this.formalName = formalName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getStartDate() {
		if(startDate != null && !startDate.equals("")) {
			if(startDate.contains("/Date")) {
				SimpleDateFormat dateFormat = new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				String filteredDate = startDate.replace("/Date(", "").replace(")/", "");

				long timestamp = Long.parseLong(filteredDate);
				Date newDate = new Date(timestamp);
				return dateFormat.format(newDate);
			}
		}
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
	public String getLastModifiedDateTime() {
		if(lastModifiedDateTime != null && !lastModifiedDateTime.equals("")) {
			if(lastModifiedDateTime.contains("/Date")) {
				SimpleDateFormat dateFormat = new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				String filteredDate = lastModifiedDateTime.replace("/Date(", "").replace(")/", "");

				long timestamp = Long.parseLong(filteredDate);
				Date newDate = new Date(timestamp);
				return dateFormat.format(newDate);
			}
		}
		return lastModifiedDateTime;
	}
	public void setLastModifiedDateTime(String lastModifiedDateTime) {
		this.lastModifiedDateTime = lastModifiedDateTime;
	}
	@Override
	public String getUserId() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void setUserId(String userId) {
		// TODO Auto-generated method stub
		
	}
}
