package com.adobe.consulting.snam.rest.pojo;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EmpCompensation implements SFOData, Serializable {

	@JsonProperty("__metadata")
	private GenericMetadata metadata;

	@JsonProperty("userId")
	private String userId;
	
	@JsonProperty("amount")
	private String amount;
	
	@JsonProperty("startDate")
	private String startDate;

	@JsonProperty("personIdExternal")
	private String personIdExternal;
	
	@JsonProperty("formalName")
	private String formalName;
	
	@JsonProperty("empPayCompRecurringNav")
	ServiceResponse empPayCompRecurringNav = null;
	
	@JsonProperty("empCompensationGroupSumCalculatedNav")
	ServiceResponse empCompensationGroupSumCalculatedNav = null;
	
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	public String getStartDate() {
		if(startDate != null && !startDate.equals("")) {
			if(startDate.contains("/Date")) {
				SimpleDateFormat dateFormat = new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				String filteredDate = startDate.replace("/Date(", "").replace(")/", "");

				long timestamp = Long.parseLong(filteredDate);
				Date newDate = new Date(timestamp);
				return dateFormat.format(newDate);
			}
		}
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
	@Override
	public GenericMetadata getMetadata() {
		return metadata;
	}
	@Override
	public void setMetadata(GenericMetadata metadata) {
		this.metadata = metadata;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPersonIdExternal() {
		return personIdExternal;
	}
	public void setPersonIdExternal(String personIdExternal) {
		this.personIdExternal = personIdExternal;
	}
	public String getFormalName() {
		return formalName;
	}
	public void setFormalName(String formalName) {
		this.formalName = formalName;
	}
	public ServiceResponse getEmpPayCompRecurringNav() {
		return empPayCompRecurringNav;
	}
	public void setEmpPayCompRecurringNav(ServiceResponse empPayCompRecurringNav) {
		this.empPayCompRecurringNav = empPayCompRecurringNav;
	}
	public ServiceResponse getEmpCompensationGroupSumCalculatedNav() {
		return empCompensationGroupSumCalculatedNav;
	}
	public void setEmpCompensationGroupSumCalculatedNav(ServiceResponse empCompensationGroupSumCalculatedNav) {
		this.empCompensationGroupSumCalculatedNav = empCompensationGroupSumCalculatedNav;
	}
}
