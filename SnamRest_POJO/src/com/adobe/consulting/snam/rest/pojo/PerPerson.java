package com.adobe.consulting.snam.rest.pojo;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PerPerson implements SFOData, Serializable {

	@JsonProperty("__metadata")
	private GenericMetadata metadata;

	@JsonProperty("personIdExternal")
	private String personIdExternal;

	@JsonProperty("startDate")
	private String startDate;
	
	@JsonProperty("lastModifiedDateTime")
	private String lastModifiedDateTime;
	
	//NAVS
	@JsonProperty("personalInfoNav")
	ServiceResponse personalInfoNav = null;
	
	public GenericMetadata getMetadata() {
		return metadata;
	}
	public void setMetadata(GenericMetadata metadata) {
		this.metadata = metadata;
	}
	public String getPersonIdExternal() {
		return personIdExternal;
	}
	public void setPersonIdExternal(String personIdExternal) {
		this.personIdExternal = personIdExternal;
	}
	public String getFormalName() {
		return formalName;
	}
	
	public String getStartDate() {
		if(startDate != null && !startDate.equals("")) {
			if(startDate.contains("/Date")) {
				SimpleDateFormat dateFormat = new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				String filteredDate = startDate.replace("/Date(", "").replace(")/", "");

				long timestamp = Long.parseLong(filteredDate);
				Date newDate = new Date(timestamp);
				return dateFormat.format(newDate);
			}
		}
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
	public String getLastModifiedDateTime() {
		if(lastModifiedDateTime != null && !lastModifiedDateTime.equals("")) {
			if(lastModifiedDateTime.contains("/Date")) {
				SimpleDateFormat dateFormat = new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				String filteredDate = lastModifiedDateTime.replace("/Date(", "").replace(")/", "");

				long timestamp = Long.parseLong(filteredDate);
				Date newDate = new Date(timestamp);
				return dateFormat.format(newDate);
			}
		}
		return lastModifiedDateTime;
	}
	public void setLastModifiedDateTime(String lastModifiedDateTime) {
		this.lastModifiedDateTime = lastModifiedDateTime;
	}
	@Override
	public String getUserId() {
		// TODO Auto-generated method stub
		return userId;
	}
	@Override
	public void setUserId(String userId) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void setFormalName(String formalName) {
		// TODO Auto-generated method stub
		
	}
	public ServiceResponse getPersonalInfoNav() {
		return personalInfoNav;
	}
	public void setPersonalInfoNav(ServiceResponse personalInfoNav) {
		this.personalInfoNav = personalInfoNav;
	}
	
}
