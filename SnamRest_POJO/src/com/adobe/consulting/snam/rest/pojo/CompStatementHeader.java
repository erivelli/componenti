package com.adobe.consulting.snam.rest.pojo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CompStatementHeader implements Serializable{

	@JsonProperty("__metadata")
	private CompStatementHeaderMetadata __metadata;
	
	@JsonProperty("externalCode")
	private String externalCode;
	
	public CompStatementHeader() {
		
	}
			
	public CompStatementHeader(String baseUri, String externalCode) {
		
		baseUri = baseUri.replace("xxx", externalCode);
		
		__metadata = new CompStatementHeaderMetadata();
		
		__metadata.setUri(baseUri);

		this.externalCode = externalCode;
	}
	
	public void setMetadata(CompStatementHeaderMetadata metadata) {
		this.__metadata = metadata;
	}
	public CompStatementHeaderMetadata getMetadata() {
		return __metadata;
	}

	public String getExternalCode() {
		return externalCode;
	}
	public void setExternalCode(String externalCode) {
		this.externalCode = externalCode;
	}
}
