package com.adobe.consulting.snam.rest.pojo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EmpCompensationGroupSumCalculated implements SFOData, Serializable {

	@JsonProperty("__metadata")
	private GenericMetadata metadata;

	@JsonProperty("personIdExternal")
	private String personIdExternal;
	
	@JsonProperty("formalName")
	private String formalName;
	
	@JsonProperty("amount")
	private String amount;
	
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	@Override
	public GenericMetadata getMetadata() {
		return metadata;
	}
	@Override
	public void setMetadata(GenericMetadata metadata) {
		this.metadata = metadata;
	}
	@Override
	public String getUserId() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String getPersonIdExternal() {
		return personIdExternal;
	}
	@Override
	public void setPersonIdExternal(String personIdExternal) {
		this.personIdExternal = personIdExternal;
	}
	@Override
	public String getFormalName() {
		return formalName;
	}
	@Override
	public void setFormalName(String formalName) {
		this.formalName = formalName;
	}
	@Override
	public void setUserId(String userId) {
		// TODO Auto-generated method stub
		
	}

}
