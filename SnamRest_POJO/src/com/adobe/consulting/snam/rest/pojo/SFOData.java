package com.adobe.consulting.snam.rest.pojo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonTypeResolver;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "__metadata.type")
@JsonSubTypes({
@JsonSubTypes.Type(value = PerPersonal.class, name = "SFOData.PerPersonal"),
@JsonSubTypes.Type(value = PerPerson.class, name = "SFOData.PerPerson"),
@JsonSubTypes.Type(value = EmpEmployment.class, name = "SFOData.EmpEmployment"),
@JsonSubTypes.Type(value = EmpJob.class, name = "SFOData.EmpJob"),
@JsonSubTypes.Type(value = EmpCompensation.class, name = "SFOData.EmpCompensation"),
@JsonSubTypes.Type(value = EmpPayCompRecurring.class, name = "SFOData.EmpPayCompRecurring"),
@JsonSubTypes.Type(value = EmpPayCompNonRecurring.class, name = "SFOData.EmpPayCompNonRecurring"),
@JsonSubTypes.Type(value = EmpCompensationGroupSumCalculated.class, name = "SFOData.EmpCompensationGroupSumCalculated"),
@JsonSubTypes.Type(value = PayScaleLevel.class, name = "SFOData.PayScaleLevel")
})
@JsonTypeResolver(NestedTypeResolver.class)
public interface SFOData extends Serializable{

	@JsonProperty("__metadata")
	GenericMetadata metadata = null;
	
	@JsonProperty("employmentNav")
	EmpEmployment employmentNav = null;
	
	@JsonProperty("compInfoNav")
	EmpCompensation compInfoNav = null;
	
	@JsonProperty("personNav")
	PerPerson personNav = null;
	
	@JsonProperty("personalInfoNav")
	PerPersonal personalInfoNav = null;
	
	@JsonProperty("empPayCompRecurringNav")
	ServiceResponse empPayCompRecurringNav = null;
	
	@JsonProperty("empPayCompNonRecurringNav")
	EmpPayCompNonRecurring empPayCompNonRecurringNav = null;
	
	@JsonProperty("payScaleLevelNav")
	PayScaleLevel payScaleLevelNav = null;
	
	String userId = null;
	String personIdExternal = null;
	String formalName = null;

	public GenericMetadata getMetadata();
	public void setMetadata(GenericMetadata metadata);
	
	public String getUserId();
	public void setUserId(String userId);
	
	public String getPersonIdExternal();
	public void setPersonIdExternal(String personIdExternal);
	
	public String getFormalName();
	public void setFormalName(String formalName);

}