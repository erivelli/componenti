package com.adobe.consulting.snam.rest.pojo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GenericMetadata implements Serializable {
	
	@JsonProperty("uri")
	private String uri;
	
	@JsonProperty("type")
	private String type;
	
	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
