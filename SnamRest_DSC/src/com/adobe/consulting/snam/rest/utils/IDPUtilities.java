package com.adobe.consulting.snam.rest.utils;

import java.io.IOException;
import java.util.Base64;

import com.adobe.idp.Document;

public class IDPUtilities {
	
	public static byte[] document2byte(Document document) throws IOException {

	    byte[] targetArray = new byte[(int) document.length()];
	    document.getInputStream().read(targetArray);
	    
		return targetArray;
	}
	
	public static String document2String(Document document) throws IOException {

	    byte[] targetArray = new byte[(int) document.length()];
	    document.getInputStream().read(targetArray);
	    
		return new String(targetArray, "UTF-8");
	}
	
	public static String document2Base64(Document document) throws IOException {

	    byte[] targetArray = new byte[(int) document.length()];
	    document.getInputStream().read(targetArray);
	    
	    String encoded = Base64.getEncoder().encodeToString(targetArray);
	    
		return encoded;
	}
	
	public static Document base642Document(String base64, String contentType) throws IOException {
	    
	    byte[] data = Base64.getDecoder().decode(base64);
	    
	    Document doc = new Document(data);
	    
	    doc.setContentType(contentType);
	    
		return doc;
	}
}
