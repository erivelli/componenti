package com.adobe.consulting.snam.rest.utils;


import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.adobe.idp.Document;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;

public class XmlUtilities {

	public static void DOMToFile(org.w3c.dom.Document domDoc,  File file) throws TransformerException, IOException {
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		Source src = new DOMSource(domDoc);

		FileOutputStream fos = new FileOutputStream(file, false);

		StreamResult dest = new StreamResult(fos);

		transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		transformer.transform(src, dest);

		fos.flush();
		fos.close();

	}

	public static org.w3c.dom.Document DOMFromFile(String path) throws ParserConfigurationException, SAXException, IOException{
		DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
		domFactory.setNamespaceAware(true);
		DocumentBuilder builder = domFactory.newDocumentBuilder();

		InputStream inputStream = new FileInputStream(path);
		org.w3c.dom.Document dom = builder.parse(new InputSource(new InputStreamReader(inputStream, "UTF-8")));

		return dom;
	}

	public static org.w3c.dom.Document DocumentToDOM(Document document) throws ParserConfigurationException, SAXException, IOException{
		DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
		domFactory.setNamespaceAware(true);
		DocumentBuilder builder = domFactory.newDocumentBuilder();

		org.w3c.dom.Document dom = builder.parse(new InputSource(new InputStreamReader(document.getInputStream(), "UTF-8")));

		return dom;
	}

	public static String XmlFileToString(String path) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException{
		DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
		domFactory.setNamespaceAware(true);
		DocumentBuilder builder = domFactory.newDocumentBuilder();

		InputStream inputStream = new FileInputStream(path);
		org.w3c.dom.Document dom = builder.parse(new InputSource(new InputStreamReader(inputStream, "UTF-8")));

		return DOMToString(dom, 4);

	}

	public static org.w3c.dom.Document deleteNodeFromDOM(org.w3c.dom.Document document, String nodePath, NamespaceContext nsContext) throws XPathExpressionException {

		XPathFactory xpf = XPathFactory.newInstance();

		XPath xpath = xpf.newXPath();

		if (nsContext != null) xpath.setNamespaceContext(nsContext);

		XPathExpression expression = xpath.compile(nodePath);

		Node nodeToDelete = (Node) expression.evaluate(document, XPathConstants.NODE);

		if(nodeToDelete != null) nodeToDelete.getParentNode().removeChild(nodeToDelete);

		return document;

	}
	public static String indent(String xml, int indentLevel) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException{

		return DOMToString(DOMFromString(xml, true), indentLevel);
	}

	public static String DOMToString(org.w3c.dom.Document document, int indentLevel) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException{
		try

		{

			XPath xPath = XPathFactory.newInstance().newXPath();
			NodeList nodeList = (NodeList) xPath.evaluate("//text()[normalize-space()='']", document, XPathConstants.NODESET);

			for (int i = 0; i < nodeList.getLength(); ++i) {
				Node node = nodeList.item(i);
				node.getParentNode().removeChild(node);
			}

			Transformer t = TransformerFactory.newInstance().newTransformer();
			t.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			//t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			t.setOutputProperty(OutputKeys.INDENT, "yes");
			t.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", String.valueOf(indentLevel));

			StringWriter stringWriter = new StringWriter();
			StreamResult streamResult = new StreamResult(stringWriter);

			t.transform(new DOMSource(document), streamResult);

			return stringWriter.toString();

		} catch (TransformerException e){

			e.printStackTrace();

		}

		return null;
	}

	public static Document DOMToDocument(org.w3c.dom.Document doc) throws XPathExpressionException, ParserConfigurationException, SAXException, IOException
	{

		String myStr = DOMToString(doc, 4);

		Document myDoc = new Document( myStr.getBytes("UTF-8"));

		return myDoc;
	}

	public static org.w3c.dom.Document NodeToDOM(Node node) throws ParserConfigurationException{
		DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
		domFactory.setNamespaceAware(true);
		DocumentBuilder builder = domFactory.newDocumentBuilder();

		org.w3c.dom.Document dom = builder.newDocument();

		Node importedNode = dom.importNode(node, true);
		dom.appendChild(importedNode);

		return dom;
	}


	public static org.w3c.dom.Document DOMFromString(String xmlString, boolean namespaceAware) throws ParserConfigurationException, UnsupportedEncodingException, SAXException, IOException
	{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		if (namespaceAware) factory.setNamespaceAware(true);

		DocumentBuilder db = factory.newDocumentBuilder();

		org.w3c.dom.Document document = db.parse(new ByteArrayInputStream(xmlString.getBytes("UTF-8")));

		return document;
	}

	public static NodeList executeXPathNodes(org.w3c.dom.Document xml, String xPathExpression, NamespaceContext nsContext, boolean verbose) throws Exception{

		NodeList nl = getNodeList(xPathExpression, xml, nsContext);

		if (nl != null) 
		{
			if (verbose) System.out.println("Utilities.executeXPathNodes - nodes found " + nl.getLength());
		}else {
			if (verbose) System.out.println("Utilities.executeXPathNodes - No nodes found for xPath expression "  + xPathExpression);
		}

		return nl;
	}

	public static Element getChild(Element parent, String name) {
		for (Node child = parent.getFirstChild(); child != null; child = child.getNextSibling()) {
			if (child instanceof Element && name.equals(child.getNodeName())) {
				return (Element) child;
			}
		}
		return null;
	}

	public static NodeList getChildren(Node parent) {
		return parent.getChildNodes();
	}

	public static  org.w3c.dom.Document newXmlFromXPath(org.w3c.dom.Document xml, String xPathExpression) throws Exception{

		NodeList nl = XmlUtilities.getNodeList(xPathExpression, xml);
		org.w3c.dom.Document newDoc = null;
		
		if (nl != null) 
		{
			DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
			domFactory.setNamespaceAware(true); 
			
			newDoc = domFactory.newDocumentBuilder().newDocument();
	
			for (int i = 0; i < nl.getLength(); ++i) {
				Node node = nl.item(i);
				
				Node importedNode = newDoc.importNode(node, true);
				
				newDoc.appendChild(importedNode);
			}

		}

		return newDoc;
	}
	
	public static  org.w3c.dom.Document newXmlFromXPath(org.w3c.dom.Document xml, String xPathExpression, String newRootName) throws Exception{

		NodeList nl = XmlUtilities.getNodeList(xPathExpression, xml);
		org.w3c.dom.Document newDoc = null;
		
		if (nl != null) 
		{
			DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
			domFactory.setNamespaceAware(true); 
			
			newDoc = domFactory.newDocumentBuilder().newDocument();
			
			Element rootElement = null;
			
			if(newRootName == null) {
				newRootName = "root";
			}
			
			rootElement = newDoc.createElement(newRootName);
			
			newDoc.appendChild(rootElement);
			
			for (int i = 0; i < nl.getLength(); ++i) {
				Node node = nl.item(i);
				
				Node importedNode = newDoc.importNode(node, true);
				
				rootElement.appendChild(importedNode);
			}

		}

		return newDoc;
	}

	public static  NodeList executeXPathNodes(org.w3c.dom.Document xml, String xPathExpression) throws Exception{

		NodeList nl = XmlUtilities.getNodeList(xPathExpression, xml);

		if (nl != null) 
		{
			//System.out.println("Utilities.executeXPathNodes - nodes found " + nl.getLength());
		}

		return nl;
	}

	public static NodeList executeXPathNodes(org.w3c.dom.Document xml, String xPathExpression, NamespaceContext nsContext) throws Exception{

		NodeList nl = getNodeList(xPathExpression, xml, nsContext);

		if (nl != null) 
		{
			//System.out.println("Utilities.executeXPathNodes - nodes found " + nl.getLength());
		}

		return nl;
	}

	public static String executeXPathString(org.w3c.dom.Document doc, String path) throws XPathExpressionException
	{

		XPathFactory factory = XPathFactory.newInstance();
		XPath xpath = factory.newXPath();

		XPathExpression expr = xpath.compile(path);
		Object obj = expr.evaluate(doc, XPathConstants.STRING);

		if(obj == null)
		{
			return null;

		}else{
			if (obj instanceof String) {
				return (String)obj;
			}else {
				return null;
			}
		}
	}

	public static String executeXPathString(Node node, String path) throws XPathExpressionException
	{

		XPathFactory factory = XPathFactory.newInstance();
		XPath xpath = factory.newXPath();

		XPathExpression expr = xpath.compile(path);
		Object obj = expr.evaluate(node, XPathConstants.STRING);

		if(obj == null)
		{
			return null;

		}else{
			if (obj instanceof String) {
				return (String)obj;
			}else {
				return null;
			}
		}
	}

	public static Integer executeXPathInteger(org.w3c.dom.Document doc, String path) throws XPathExpressionException
	{

		XPathFactory factory = XPathFactory.newInstance();
		XPath xpath = factory.newXPath();

		XPathExpression expr = xpath.compile(path);
		Object obj = expr.evaluate(doc, XPathConstants.STRING);

		if(obj == null)
		{
			return null;

		}else{
			if (obj instanceof String) {
				return Integer.parseInt((String)obj);
			}else {
				return null;
			}
		}
	}

	public static org.w3c.dom.Document checkCreateNodePath(org.w3c.dom.Document doc, String xPath) throws XPathExpressionException, TransformerFactoryConfigurationError, TransformerException{

		String fullPath = "";
		String minPath = "";


		StringTokenizer st = new StringTokenizer(xPath, "/", false);

		while(st.hasMoreTokens()) {
			String szNode = st.nextToken();

			minPath = fullPath;
			fullPath += ("/" + szNode);

			Node n  = getSingleNode(fullPath, doc);

			if (n == null) {

				Element anchor = null;

				if (minPath.equals("")) {
					anchor = doc.getDocumentElement();
				}else{
					anchor = (Element) getSingleNode(minPath, doc);
				}

				Element newEle = doc.createElement(szNode);
				doc.importNode(newEle, true);
				anchor.appendChild(newEle);
			}
		}

		return doc;

	}

	public static NodeList getNodeList(String path, Node node) throws XPathExpressionException
	{
		NodeList ret = null;

		XPathFactory factory = XPathFactory.newInstance();
		XPath xpath = factory.newXPath();

		XPathExpression expr = xpath.compile(path);
		ret = (NodeList) expr.evaluate(node, XPathConstants.NODESET);

		return ret;
	}

	public static NodeList getNodeList(String path, Node node, NamespaceContext nsContext) throws XPathExpressionException
	{
		NodeList ret = null;

		XPathFactory factory = XPathFactory.newInstance();
		XPath xpath = factory.newXPath();

		if(nsContext != null) xpath.setNamespaceContext(nsContext);

		XPathExpression expr = xpath.compile(path);
		ret = (NodeList) expr.evaluate(node, XPathConstants.NODESET);

		return ret;
	}

	public static NodeList getNodeList(String path, org.w3c.dom.Document doc, NamespaceContext nsContext) throws XPathExpressionException
	{
		NodeList ret = null;

		XPathFactory factory = XPathFactory.newInstance();

		XPath xpath = factory.newXPath();

		if(nsContext != null) xpath.setNamespaceContext(nsContext);

		XPathExpression expr = xpath.compile(path);
		ret = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);

		return ret;
	}

	public static NodeList getNodeList(String path, org.w3c.dom.Document doc) throws XPathExpressionException
	{
		NodeList ret = null;

		XPathFactory factory = XPathFactory.newInstance();
		XPath xpath = factory.newXPath();

		XPathExpression expr = xpath.compile(path);
		ret = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);

		return ret;
	}

	public static Node getSingleNode(String path, org.w3c.dom.Document doc) throws XPathExpressionException
	{
		Node ret = null;

		XPathFactory factory = XPathFactory.newInstance();
		XPath xpath = factory.newXPath();

		XPathExpression expr = xpath.compile(path);
		ret = (Node) expr.evaluate(doc, XPathConstants.NODE);

		return ret;
	}

	public static String getChildTextNode(Node father, String childName)
	{
		String ret = null;

		NodeList lista = father.getChildNodes();
		for (int i = 0; i< lista.getLength();i++)
		{
			Node n=  (Node) lista.item(i);
			if (n.getNodeName().equals(childName)) 
			{
				ret = n.getTextContent();
				break;
			}
		}

		return ret;
	}

	public static String getStringAttribute(Node node, String attributeName)
	{
		String value = ((Element)node).getAttribute(attributeName);

		return value;
	}

	public static Integer getIntegerAttribute(Node node, String attributeName)
	{
		String value = ((Element)node).getAttribute(attributeName);

		if(value == null)
		{
			return null;

		}else{
			if (value instanceof String) {
				return Integer.parseInt(value);
			}else {
				return null;
			}
		}
	}

	public static Short getShortAttribute(Node node, String attributeName)
	{
		String value = ((Element)node).getAttribute(attributeName);

		if(value == null)
		{
			return null;

		}else{
			if (value instanceof String) {
				return Short.parseShort(value);
			}else {
				return null;
			}
		}
	}

	public static String getTextNode(String path, org.w3c.dom.Document doc, boolean nullIfEmpty) throws XPathExpressionException
	{

		XPathFactory factory = XPathFactory.newInstance();
		XPath xpath = factory.newXPath();

		XPathExpression expr = xpath.compile(path);
		Object obj = expr.evaluate(doc, XPathConstants.NODE);

		if(obj == null)
		{
			return null;

		}else{
			if (((Node) obj).getTextContent().equals("")){
				if(nullIfEmpty){
					return null;
				}
			}
			return ((Node) obj).getTextContent();
		}
	}

	public static Boolean getBooleanAttribute(Node node, String attributeName)
	{
		String value = ((Element)node).getAttribute(attributeName);

		if(value == null)
		{
			return false;

		}else{
			if (value instanceof String) {
				return Boolean.valueOf(value);
			}else {
				return false;
			}
		}
	}


	public static String serializeObject(Object object) throws JAXBException, IOException {
		JAXBContext ctx = JAXBContext.newInstance(object.getClass());

		Marshaller m = ctx.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		StringWriter sw = new StringWriter();
		m.marshal(object, sw);
		sw.close();

		return sw.toString();
	}

	public static String serializeObjectXStream(Object source) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {

		XStream xstream = new XStream(new StaxDriver());

		return indent(xstream.toXML(source), 4);

	}

	public static Object deserializeObjectXStream(String xml) {

		XStream xstream = new XStream(new StaxDriver());

		xstream.allowTypesByWildcard(new String[] {
				"com.adobe.**", "org.apache.**", "java.**"
		});

		return xstream.fromXML(xml);
	}	

	public static Object deserializeObject(String xml, Class className) throws ParserConfigurationException, UnsupportedEncodingException, SAXException, IOException, ClassNotFoundException, JAXBException {

		DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		org.w3c.dom.Document document = db.parse(new ByteArrayInputStream(xml.getBytes("UTF-8")));

		JAXBContext jaxbContext = JAXBContext.newInstance(className);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

		JAXBElement root = (JAXBElement) jaxbUnmarshaller.unmarshal(new StreamSource(new StringReader(xml)), className);

		return root.getValue();
	}

	public static String serializeObjectWithClasses(Object source, Class ...additionalClasses) throws JAXBException, IOException, ParserConfigurationException, SAXException {
		JAXBContext ctx = JAXBContext.newInstance(additionalClasses);

		//MOXY
		//JAXBContext ctx = JAXBContextFactory.createContext(additionalClasses, null);

		Marshaller m = ctx.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		QName qName = new QName(source.getClass().getTypeName());
		JAXBElement root = new JAXBElement(qName, source.getClass(), source);

		StringWriter sw = new StringWriter();
		m.marshal(root, sw);
		sw.close();

		return sw.toString();

	}

	public static Object deserializeObjectWithClasses(String xml, Class ...additionalClasses) throws ParserConfigurationException, UnsupportedEncodingException, SAXException, IOException, ClassNotFoundException, JAXBException {

		DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		org.w3c.dom.Document document = db.parse(new ByteArrayInputStream(xml.getBytes("UTF-8")));

		String className = document.getDocumentElement().getNodeName();

		JAXBContext ctx = JAXBContext.newInstance(additionalClasses);

		//MOXY
		//JAXBContext ctx = JAXBContextFactory.createContext(additionalClasses, null);

		Unmarshaller jaxbUnmarshaller = ctx.createUnmarshaller();

		JAXBElement root = (JAXBElement) jaxbUnmarshaller.unmarshal(new StreamSource(new StringReader(xml)), Class.forName(className));

		return root.getValue();
	}

	public static org.w3c.dom.Document applyXslt(org.w3c.dom.Document dom, InputStream xsltTemplate) throws TransformerException {

		// Create transformer factory
		TransformerFactory factory = TransformerFactory.newInstance();

		// Use the template to create a transformer
		Transformer xformer = factory.newTransformer(new StreamSource(xsltTemplate));

		// Prepare the input and output files
		Source source = new DOMSource(dom);
		DOMResult result = new DOMResult();

		// Apply the xsl file to the source file and write the result
		// to the output file
		xformer.transform(source, result);

		return (org.w3c.dom.Document) result.getNode();
	}

	public static XmlSyntaxValidationErrorHandler validateSyntax(String xmlString, boolean namespaceAware) throws ParserConfigurationException, SAXException, IOException {

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setValidating(false);

		DocumentBuilder db = factory.newDocumentBuilder();

		if (namespaceAware) factory.setNamespaceAware(true);

		XmlSyntaxValidationErrorHandler errorHandler = new XmlSyntaxValidationErrorHandler();

		db.setErrorHandler(errorHandler); 

		org.w3c.dom.Document document = db.parse(new ByteArrayInputStream(xmlString.getBytes("UTF-8")));

		return errorHandler;
	}

	public static class XmlSyntaxValidationErrorHandler implements ErrorHandler {

		List<String> warnings = new ArrayList<String>();
		List<String> errors = new ArrayList<String>();
		List<String> fatalErrors = new ArrayList<String>();

		int validationFailures = 0;

		public void warning(SAXParseException e) throws SAXException {
			warnings.add(e.getMessage());
			validationFailures++;
		}

		public void error(SAXParseException e) throws SAXException {
			warnings.add(e.getMessage());
			validationFailures++;
		}

		public void fatalError(SAXParseException e) throws SAXException {
			fatalErrors.add(e.getMessage());
			validationFailures++;
		}

		public List<String> getWarnings() {
			return warnings;
		}

		public List<String> getErrors() {
			return errors;
		}

		public List<String> getFatalErrors() {
			return fatalErrors;
		}

		public int getValidationFailures() {
			return validationFailures;
		}
	}

	public Node createNodesPath(org.w3c.dom.Document doc, Node parent, String path)
	{
		Node newNode = null;

		// grab the next node name in the xpath; or return parent if empty
		String[] partsOfXPath = path.trim().split("/");
		String nextNodeInXPath = partsOfXPath[0];
		if (nextNodeInXPath == null || nextNodeInXPath.isEmpty())
			return parent;

		// get or create the node from the name
		NodeList nd = ((Element)parent).getElementsByTagName(nextNodeInXPath);

		if(nd.getLength() == 0) {
			newNode = parent.appendChild(doc.createElement(nextNodeInXPath));
		}else {
			newNode = nd.item(0);
		}

		// rejoin the remainder of the array as an xpath expression and recurse
		String rest = String.join("/", Arrays.copyOfRange(partsOfXPath, 1, partsOfXPath.length));
		return createNodesPath(doc, newNode, rest);
	}

}
