package com.adobe.consulting.snam.rest.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "source")
public class ExternalDataSource implements Serializable, Cloneable{
	
	@XmlAttribute(name = "type")
	protected String m_type;
	
	@XmlAttribute(name = "service")
	protected String m_service;
	
	@XmlAttribute(name = "enabled")
	protected boolean m_enabled;
	
	protected QueryBean query;
	
	@XmlElementWrapper(name = "operations")
	@XmlElement(name = "operation")
	private List<OperationNodeMerge> m_operations;
	
	public String getType() {
		return m_type;
	}
	public void setService(String service) {
		this.m_service = service;
	}
	public String getService() {
		return m_service;
	}
	public void setType(String type) {
		this.m_type = type;
	}
	public boolean isEnabled() {
		return m_enabled;
	}
	public void setEnabled(boolean enabled) {
		this.m_enabled = enabled;
	}
	public QueryBean getQuery() {
		return query;
	}
	public void setQuery(QueryBean query) {
		this.query = query;
	}
	public List<OperationNodeMerge> getOperations() {
		return m_operations;
	}
	public void setOperations(List<OperationNodeMerge> operations) {
		this.m_operations = operations;
	}
	
	public ExternalDataSource clone() {
		try {
			
			ExternalDataSource p = (ExternalDataSource) super.clone();
			
			p.setQuery(p.getQuery().clone());
			
			List<OperationNodeMerge> clone = new ArrayList<OperationNodeMerge>(p.getOperations().size());
		    for (OperationNodeMerge item : p.getOperations()) clone.add(item.clone());
		    
		    p.setOperations(clone);
		    
			return p;
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}
	}
	
}
