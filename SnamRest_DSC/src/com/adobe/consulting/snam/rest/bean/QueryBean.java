package com.adobe.consulting.snam.rest.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "query")
public class QueryBean implements Serializable, Cloneable{
	
	@XmlAttribute(name = "enabled")
	boolean m_enabled = false;
	
	String format = null;
	String filter = null;
	String select = null;
	String expand = null;
	String fromDate = null;
	String toDate = null;
	String skiptoken = null;
	
	@XmlElementWrapper(name = "parameters")
	@XmlElement(name = "parameter")
	protected List<QueryParameterBean> m_parameters = new ArrayList<QueryParameterBean>();
	
	public boolean getEnabled() {
		return m_enabled;
	}
	public void setEnabled(boolean enabled) {
		this.m_enabled = enabled;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	public String getFilter() {
		return filter;
	}
	public void setFilter(String filter) {
		this.filter = filter;
	}
	public String getSelect() {
		return select;
	}
	public void setSelect(String select) {
		this.select = select;
	}
	public String getExpand() {
		return expand;
	}
	public void setExpand(String expand) {
		this.expand = expand;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getSkiptoken() {
		return skiptoken;
	}
	public void setSkiptoken(String skiptoken) {
		this.skiptoken = skiptoken;
	}
	
	public List<QueryParameterBean> getParameters() {
		return m_parameters;
	}

	public void setParameters(List<QueryParameterBean> parameters) {
		this.m_parameters = parameters;
	}	
	
	public QueryBean clone() {
		try {
			
			QueryBean p = (QueryBean) super.clone();
			
			List<QueryParameterBean> clone = new ArrayList<QueryParameterBean>(p.getParameters().size());
		    for (QueryParameterBean item : p.getParameters()) clone.add(item.clone());
		    
		    p.setParameters(clone);
		    
			return p;
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}
	}
	
}
