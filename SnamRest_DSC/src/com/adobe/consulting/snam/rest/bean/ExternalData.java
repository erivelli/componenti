package com.adobe.consulting.snam.rest.bean;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "externalData")
public class ExternalData {
	
	@XmlElementWrapper(name = "sources")
	@XmlElement(name = "source")
	protected List<ExternalDataSource> m_sources = new ArrayList<ExternalDataSource>();
	
	public List<ExternalDataSource> getSources() {
		return m_sources;
	}

	public void setSources(List<ExternalDataSource> sources) {
		this.m_sources = sources;
	}	
	
}
