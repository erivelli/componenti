package com.adobe.consulting.snam.rest.bean;

import com.adobe.livecycle.SinceLC;
import java.io.Serializable;
import java.util.List;

public class XmlDocumentInfoBean
  implements Serializable
{
  private static final long serialVersionUID = 4261292560336427119L;
  private String m_rootName;
  private String m_repeatingElementName;
  private List m_columnNameMappings;
  private boolean m_escapeIllegalCharacters = true;
  public static final String ROOT_DEFAULT_NAME = "root";
  public static final String ELEMENT_DEFAULT_NAME = "element";
  public static final String STYLE_ELEMENT = "column names as elements";
  public static final String STYLE_ATTRIBUTE = "column names as attributes";
  public static final String NAME_ATTRIBUTE = "name";
  public static final String VALUE_ATTRIBUTE = "value";
  public static final String TYPE_ATTRIBUTE = "type";
  public static final String MAPPING_INDEX = "column_index";
  public static final String MAPPING_COLUMN_NAME = "column_name";
  public static final String MAPPING_ELEMENT_NAME = "element_name";
  
  public XmlDocumentInfoBean(String rootName, String repeatingElementName, List columnNameMappings, @SinceLC("9.0.0") boolean escapeIllegalCharacters)
  {
    this.m_rootName = rootName;
    this.m_repeatingElementName = repeatingElementName;
    this.m_columnNameMappings = columnNameMappings;
    this.m_escapeIllegalCharacters = escapeIllegalCharacters;
  }
  
  public String getRepeatingElementName() {
    return this.m_repeatingElementName;
  }
  
  public void setRepeatingElementName(String repeatingElementName) {
    this.m_repeatingElementName = repeatingElementName;
  }
  
  public String getRootName() {
    return this.m_rootName;
  }
  
  public void setRootName(String rootName) {
    this.m_rootName = rootName;
  }
  
  public List getColumnNameMappings() {
    return this.m_columnNameMappings;
  }
  
  public void setColumnNameMappings(List columnNameMappings) {
    this.m_columnNameMappings = columnNameMappings;
  }
  

  @SinceLC("9.0.0")
  public boolean isEscapeIllegalCharacters()
  {
    return this.m_escapeIllegalCharacters;
  }
  
  @SinceLC("9.0.0")
  public void setEscapeIllegalCharacters(boolean escapeIllegalCharacters) {
    this.m_escapeIllegalCharacters = escapeIllegalCharacters;
  }
  

  public String toString()
  {
    return 
    

      "root: " + getRootName() + "; element: " + getRepeatingElementName() + "; mapping: " + getColumnNameMappings() + "; escape illegal characters: " + this.m_escapeIllegalCharacters;
  }
}

