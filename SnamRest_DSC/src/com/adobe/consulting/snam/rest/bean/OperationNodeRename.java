package com.adobe.consulting.snam.rest.bean;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;

public class OperationNodeRename implements Serializable {

	@XmlAttribute(name="target")
	String m_target = null;
	
	@XmlAttribute(name="oldName")
	String m_oldName = null;
	
	@XmlAttribute(name="newName")
	String m_newName = null;
	
	public OperationNodeRename() {
		
	}
	
	public OperationNodeRename(
			String target, 
			String oldName,
			String newName) {
		
		this.m_target = target;
		this.m_oldName = oldName;
		this.m_newName = newName;
		
	}
	
	public String getTarget() {
		return m_target;
	}

	public void setTarget(String target) {
		this.m_target = target;
	}

	public String getOldName() {
		return m_oldName;
	}

	public void setOldName(String oldName) {
		this.m_oldName = oldName;
	}

	public String getNewName() {
		return m_newName;
	}

	public void setNewName(String newName) {
		this.m_newName = newName;
	}

}
