package com.adobe.consulting.snam.rest.bean;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;

public class OperationNodeMerge implements Serializable, Cloneable {

	@XmlAttribute(name="type")
	private String m_type = null;
	
	@XmlAttribute(name="target")
	private String m_target = null;
	
	@XmlAttribute(name="destination")
	private String m_destination = null;
	
	@XmlAttribute(name="rename")
	private String m_rename = null;
	
	@XmlAttribute(name="exclude")
	private String m_exclude = null;
	
	@XmlAttribute(name="function")
	private String m_function = null;
	
	@XmlAttribute(name="functionParameters")
	private String m_functionParameters = null;
	
	public OperationNodeMerge() {
		
	}
	
	public OperationNodeMerge(
			String type,
			String target,
			String destination,
			String rename,
			String exclude,
			String function,
			String functionParameters) {
		
		this.m_type = type; 
		this.m_target = target; 
		this.m_destination = destination; 
		this.m_rename = rename; 
		this.m_exclude = exclude; 
		this.m_function = function; 
		this.m_functionParameters = functionParameters; 
		
	}
	
	public String getType() {
		return m_type;
	}
	public void setType(String type) {
		this.m_type = type;
	}
	public String getTarget() {
		return m_target;
	}
	public void setTarget(String target) {
		this.m_target = target;
	}
	public String getDestination() {
		return m_destination;
	}
	public void setDestination(String destination) {
		this.m_destination = destination;
	}
	public String getRename() {
		return m_rename;
	}
	public void setRename(String rename) {
		this.m_rename = rename;
	}
	public String getExclude() {
		return m_exclude;
	}
	public void setExclude(String exclude) {
		this.m_exclude = exclude;
	}
	public String getFunction() {
		return m_function;
	}
	public void setFunction(String function) {
		this.m_function = function;
	}
	public String getFunctionParameters() {
		return m_functionParameters;
	}
	public void setFunctionParameters(String functionParameters) {
		this.m_functionParameters = functionParameters;
	}
	
	public OperationNodeMerge clone() {
		try {
			OperationNodeMerge p = (OperationNodeMerge) super.clone();
			return p;
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}
	}
	
}
