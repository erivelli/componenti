package com.adobe.consulting.snam.rest.bean;

import java.util.LinkedHashMap;
import java.util.Map.Entry;

public class StatementHolder {
	String originalStatement=null;
	String completedStatement=null;
	LinkedHashMap<Integer, Integer> argumentsPositions = new LinkedHashMap<Integer, Integer>();
	LinkedHashMap<Integer, String> replacements = new LinkedHashMap<Integer, String>();
	
	public StatementHolder(String statement) {
		this.originalStatement = statement;
		
		int counter = 1;
		
		int indexFound = statement.indexOf("?");
		
		while (indexFound >= 0) {
			argumentsPositions.put(counter, indexFound);
			indexFound = statement.indexOf("?", indexFound + 1);
			counter++;
		}
	}
	
	public void setArgument(int index, String value) {
		
		replacements.put(index, value);
	}
	
	public String getCompletedStatement() {
		
		completedStatement = originalStatement;
		
		if(argumentsPositions.size() > 0) {
			
			for (Entry<Integer, String> replacement : replacements.entrySet()) {
				
				//Integer key = replacement.getKey();
				
				//Integer position = argumentsPositions.get(key);
				
				completedStatement = completedStatement.replaceFirst("\\?", replacement.getValue());
			}
			
			return completedStatement;
		}
		
		return originalStatement;
	}
}
