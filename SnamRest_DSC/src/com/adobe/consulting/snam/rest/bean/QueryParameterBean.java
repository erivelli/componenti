package com.adobe.consulting.snam.rest.bean;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "parameter")
public class QueryParameterBean implements Cloneable{
	
	@XmlAttribute(name = "index")
	protected int m_index;
	
	@XmlAttribute(name = "target")
	protected String m_target;

	@XmlAttribute(name = "modify")
	protected String m_modify;
	
	@XmlAttribute(name = "type")
	protected String m_type;
	
	@XmlAttribute(name = "value")
	protected String m_value;
	
	public int getIndex() {
		return m_index;
	}
	public void setIndex(int index) {
		this.m_index = index;
	}
	public String getTarget() {
		return m_target;
	}
	public void setTarget(String target) {
		this.m_target = target;
	}
	public String getModify() {
		return m_modify;
	}
	public void setModify(String modify) {
		this.m_modify = modify;
	}
	public String getType() {
		return m_type;
	}
	public void setType(String type) {
		this.m_type = type;
	}
	public String getValue() {
		return m_value;
	}
	public void setValue(String value) {
		this.m_value = value;
	}
	
	public QueryParameterBean clone() {
		try {
			QueryParameterBean p = (QueryParameterBean) super.clone();
			return p;
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}
	}

}
