package com.adobe.consulting.snam.rest.service;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.adobe.consulting.snam.rest.bean.ConnectionBean;
import com.adobe.consulting.snam.rest.bean.ExternalData;
import com.adobe.consulting.snam.rest.bean.ExternalDataSource;
import com.adobe.consulting.snam.rest.bean.OperationNodeMerge;
import com.adobe.consulting.snam.rest.bean.QueryBean;
import com.adobe.consulting.snam.rest.pojo.Attachment;
import com.adobe.consulting.snam.rest.pojo.AttachmentIdentifier;
import com.adobe.consulting.snam.rest.pojo.CompStatementDetails;
import com.adobe.consulting.snam.rest.pojo.CompStatementDetailsNav;
import com.adobe.consulting.snam.rest.pojo.CompStatementHeader;
import com.adobe.consulting.snam.rest.pojo.EmpCompensation;
import com.adobe.consulting.snam.rest.pojo.EmpEmployment;
import com.adobe.consulting.snam.rest.pojo.EmpJob;
import com.adobe.consulting.snam.rest.pojo.PerPersonal;
import com.adobe.consulting.snam.rest.pojo.SFOData;
import com.adobe.consulting.snam.rest.pojo.ServiceResult;
import com.adobe.consulting.snam.rest.pojo.UpsertResponse;
import com.adobe.consulting.snam.rest.pojo.UpsertResult;
import com.adobe.consulting.snam.rest.utils.IDPUtilities;
import com.adobe.consulting.snam.rest.utils.XmlUtilities;

public class Test {

	public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException, XPathExpressionException, UnsupportedOperationException, NamingException, SQLException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, ClassNotFoundException, JAXBException {
		
		SnamRestServiceImpl service = new SnamRestServiceImpl();
		
		ExternalDataSource externalDataSource = new ExternalDataSource();
		
		externalDataSource.setOperations(new ArrayList<OperationNodeMerge>());
		
		externalDataSource.getOperations().add(new OperationNodeMerge("A", "B", "C", "D", "E", "F", "G"));
		
		externalDataSource.setQuery(new QueryBean());
		
		externalDataSource.getQuery().setFilter("ciao ?");
		
		ExternalDataSource externalDataSourceCloned = externalDataSource.clone();
		
		externalDataSourceCloned.getOperations().get(0).setDestination("culo");
		
		externalDataSourceCloned.getQuery().setFilter("ciao EDO");
		
		Path fileName = Paths.get("/Users/erivelli/Desktop/BDI/LCManager/file.xml");
         
        byte[] bytes = Files.readAllBytes(fileName);
        
        Document dom = XmlUtilities.DOMFromFile("/Users/erivelli/Desktop/BDI/LCManager/file.xml");
		
		Object obj = service.deserializeObject(dom, "com.adobe.consulting.snam.rest.bean.ExternalData");

		
//		service.setProxyHost("localhost");
//		service.setProxyPort("80");
//		service.setProxyUser("user");
//		service.setProxyPassword("password");

		String newstr = "2020-10-01T02:00:00".replaceAll("T.*", "T00:00:00");
		
		System.out.println(newstr);
		
		System.out.println(newstr);

		String userId = null;
		String personIdExternal = null;
		ServiceResult result = null;
		List<SFOData> sFOData = null;
		
		//PerPersonal: empJob -> "employmentNav/personNav/personalInfoNav"
		//employmentNav/personNav/personalInfoNav/personIdExternal,employmentNav/personNav/personalInfoNav/formalName
		
		//EmpPayCompRecurring: empJob -> "employmentNav/compInfoNav/empPayCompRecurringNav"

		ConnectionBean connectionBean = new ConnectionBean();
		
		connectionBean.setJndi("java:/IDP_DS");
		connectionBean.setUrl("https://api2preview.sapsf.eu");
		connectionBean.setUser("adminSCPI@snamspaT1");
		connectionBean.setPassword("snam2021");
		
		QueryBean query = new QueryBean();
		
		query.setFormat("json");
		query.setToDate("9999-12-31");
		query.setFilter("endDate in datetime'2021-04-30T00:00:00' and userId eq '831'");
		query.setSelect("payScaleLevelNav/externalName_it_IT,startDate,payScaleLevel,eventReason,company,userId,employmentNav/personNav/personalInfoNav/personIdExternal,employmentNav/personNav/personalInfoNav/formalName");
		query.setExpand("payScaleLevelNav,employmentNav/personNav/personalInfoNav");
		
		//"startDate gt datetime'2020-01-01T00:00:00' and eventReason eq 'MIMP006' and userId eq '831'"
		
		
		
		service.setVerbose(true);

		//EMP JOB
		result = service.callService(
				"empJob",
				connectionBean,
				query);
		
		
		int bufferedCalls = 1;
		
		if(result.getError() != null) {
			System.err.println(result.getError().getMessage().getValue());
			return;
		}

		sFOData = result.getResponse().getRecords();
		
		org.w3c.dom.Document response = service.obj2XmlDocument(result.getResponse());
		System.out.println(XmlUtilities.DOMToString(response, 4));
		
		System.out.println("EmpJob count: " + sFOData.size());

		System.out.println(bufferedCalls);

		for (SFOData sfo : sFOData) {
			//System.out.println(empJob.getStartDate());
			
			String serialized = service.obj2XmlString(sfo);
		
			System.out.println(serialized);
			
			EmpJob empJob = (EmpJob) sfo;
			PerPersonal perPersonal = (PerPersonal) empJob.getEmploymentNav().getPersonNav().getPersonalInfoNav().getRecords().get(0);

			System.out.println("userId: " + sfo.getUserId());
			
			
			System.out.println("personIdExternal: " + perPersonal.getPersonIdExternal());
			System.out.println("formalName: " + perPersonal.getFormalName());
			System.out.println("dateTime: " + empJob.getStartDate());

			userId = empJob.getUserId();
		}
		
		

		while (result.getResponse().isHasNext()) {

			bufferedCalls++;

			System.out.println(bufferedCalls);

			String idNextToken = result.getResponse().getIdNextToken();

			result = service.empJob(
					"https://api2preview.sapsf.eu",
					"adminSCPI@snamspaT1",
					"snam2021", 
					"json", 
					null,
					"startDate,payScaleLevel,payGrade,eventReason,company,userId", 
					null, 				
					null,
					null,
					idNextToken);

			sFOData = result.getResponse().getRecords();

			System.out.println("EmpJob count: " + sFOData.size());
			
			for (SFOData sfo : sFOData) {

				EmpJob empJob = (EmpJob) sfo;

				//System.out.println("userId: " + empJob.getUserId());
			}

			//jsonString = mapper.writeValueAsString(result);

		}

		//EMP EMPLOYMENT
		result = service.empEmployment(
				"https://api2preview.sapsf.eu",
				"adminSCPI@snamspaT1",
				"snam2021", 
				"json", 
				null, //"userId eq '" + userId + "'",
				"userId, personIdExternal",
				null,
				null,
				null,
				null);

		sFOData = result.getResponse().getRecords();

		for (SFOData sfo : sFOData) {

			EmpEmployment empEmployment = (EmpEmployment) sfo;

			System.out.println("userId: " + empEmployment.getUserId() + ", personIdExternal: " + empEmployment.getPersonIdExternal());

			personIdExternal = empEmployment.getPersonIdExternal();
		}

//
//		//PER PERSONAL
//		result = service.perPersonal(
//				"https://api2preview.sapsf.eu",
//				"adminSCPI@snamspaT1",
//				"snam2021", 
//				"json", 
//				null,
//				//"personIdExternal eq '" + personIdExternal + "'",
//				"formalName, startDate, lastModifiedDateTime",
//				null,
//				null);
//
//		sFOData = result.getResponse().getRecords();
//		
//		System.out.println("PerPersonal count: " + sFOData.size());
//
//		for (SFOData sfo : sFOData) {
//
//			PerPersonal perPersonal = (PerPersonal) sfo;
//
//			System.out.println("formalName: " + perPersonal.getFormalName());
//		}
//
		//EMP COMPENSATION
		result = service.empCompensation(
				"https://api2preview.sapsf.eu",
				"adminSCPI@snamspaT1",
				"snam2021", 
				"json", 
				null, //"startDate in datetime'2021-05-01T00:00:00' and personIdExternal eq '" + personIdExternal + "'",
				null,
				null,//"EmpCompensationGroupSumCalculated",
				null,
				null,
				null);

		if(result.getError() == null) {

			sFOData = result.getResponse().getRecords();

			for (SFOData sfo : sFOData) {

				EmpCompensation empCompensation = (EmpCompensation) sfo;

				System.out.println("StartDate: " + empCompensation.getStartDate());
			}
		}else {
			System.err.println("Errore durante la chiamata a empCompensation - code: " + result.getError().getCode() + ", Message: " + result.getError().getMessage().getValue());
		}



		//EMP PAY COMP NOT RECURRING
		result = service.empPayCompNonRecurring(
				"https://api2preview.sapsf.eu",
				"adminSCPI@snamspaT1",
				"snam2021", 
				"json", 
				null,
				null,
				null,
				null,
				null,
				null);

		sFOData = result.getResponse().getRecords();

		for (SFOData sfo : sFOData) {

			EmpEmployment empEmployment = (EmpEmployment) sfo;

			System.out.println("personIdExternal: " + empEmployment.getPersonIdExternal());

			personIdExternal = empEmployment.getPersonIdExternal();
		}
		
		//------------------------------------

		String baseUri = "https://api2preview.sapsf.eu/odata/v2/cust_Comp_Statement_Details(cust_Comp_Statement_Header_externalCode='xxx',externalCode='yyy')";

		//PHASE1
		CompStatementHeader compStatementHeader = new CompStatementHeader(baseUri, "100");
		
		//PHASE2
		CompStatementDetailsNav compStatementDetailsNav = new CompStatementDetailsNav(baseUri, "100", "1", null);
		Attachment attachment = new Attachment();

		com.adobe.idp.Document pdf = new com.adobe.idp.Document(new File("/Users/erivelli/Desktop/in/2 A- Sez 1 - Contenuto RdO.pdf"), false);

		pdf.setMaxInlineSize(100000000);

		attachment.setUserId("100");
		attachment.setExternalId("1");
		attachment.setFileName("lettera.pdf");
		attachment.setModule("GENERIC_OBJECT");
		attachment.setDescription("Test Adobe");
		attachment.setFileContent(IDPUtilities.document2Base64(pdf));
		attachment.setViewable(true);
		attachment.setDeletable(false);

		UpsertResult upsertResult;
		UpsertResponse upsertResponse;
		String key_1;
		String key_2;
		String attachment_id;
		
		//UPSTART 1
		upsertResult = service.upsertPhase1(
				"https://api2preview.sapsf.eu",
				"adminSCPI@snamspaT1",
				"snam2021", 
				compStatementHeader);
		
		upsertResponse = upsertResult.getResponse().get(0);

		if(!upsertResponse.getStatus().equals("OK")){
			
			System.err.println("Errore durante la call a upsertPhase1 - code: " + upsertResponse.getHttpCode() + ", message: " + upsertResponse.getMessage());
			return;
			
		}else {
			key_1 = upsertResponse.getKey();
			System.out.println("upsertPhase1 Key is " + key_1);
		}
		
		//UPSTART 2
		upsertResult = service.upsertPhase2(
				"https://api2preview.sapsf.eu",
				"adminSCPI@snamspaT1",
				"snam2021", 
				attachment);
		
		upsertResponse = upsertResult.getResponse().get(0);

		if(!upsertResponse.getStatus().equals("OK")){
			
			System.err.println("Errore durante la call a upsertPhase2 - code: " + upsertResponse.getHttpCode() + ", message: " + upsertResponse.getMessage());
			return;
			
		}else {
			key_2 = upsertResponse.getKey();
			System.out.println("upsertPhase2 Key is " + key_2);
			
			attachment_id = key_2.substring(key_2.indexOf("attachmentId=") + 13);
			System.out.println("upsertPhase2 attachmentId is " + attachment_id);
		}
		
		//SET NEW ATTACHMENT KEY
		AttachmentIdentifier attachmentIdentifier = new AttachmentIdentifier();
		attachmentIdentifier.getMetadata().setUri("Attachment(attachmentId=" + attachment_id + ")");
		compStatementDetailsNav.setCust_StatementNav(attachmentIdentifier);
		
		//UPSTART 3
		upsertResult = service.upsertPhase3(
				"https://api2preview.sapsf.eu",
				"adminSCPI@snamspaT1",
				"snam2021", 
				compStatementDetailsNav);
		
		upsertResponse = upsertResult.getResponse().get(0);

		if(!upsertResponse.getStatus().equals("OK")){
			
			System.err.println("Errore durante la call a upsertPhase3 - code: " + upsertResponse.getHttpCode() + ", message: " + upsertResponse.getMessage());
			return;
			
		}else {
			key_2 = upsertResponse.getKey();
		}

		System.out.println("UPSERT COMPLETED");
	}

}
