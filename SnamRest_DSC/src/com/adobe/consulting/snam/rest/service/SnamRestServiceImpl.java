package com.adobe.consulting.snam.rest.service;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.ProxyAuthenticationStrategy;
import org.apache.log4j.Logger;
import org.jboss.resteasy.client.jaxrs.BasicAuthentication;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.jboss.resteasy.client.jaxrs.engines.ApacheHttpClient43Engine;
import org.jboss.resteasy.client.jaxrs.engines.ApacheHttpClient4Engine;
import org.jboss.resteasy.client.jaxrs.engines.URLConnectionEngine;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.adobe.consulting.jdbc.Sql2XmlHelper;
import com.adobe.consulting.snam.rest.bean.ConnectionBean;
import com.adobe.consulting.snam.rest.bean.ExternalData;
import com.adobe.consulting.snam.rest.bean.ExternalDataSource;
import com.adobe.consulting.snam.rest.bean.OperationNodeMerge;
import com.adobe.consulting.snam.rest.bean.OperationNodeRename;
import com.adobe.consulting.snam.rest.bean.QueryBean;
import com.adobe.consulting.snam.rest.bean.QueryParameterBean;
import com.adobe.consulting.snam.rest.bean.StatementHolder;
import com.adobe.consulting.snam.rest.pojo.Attachment;
import com.adobe.consulting.snam.rest.pojo.CompStatementDetails;
import com.adobe.consulting.snam.rest.pojo.CompStatementDetailsNav;
import com.adobe.consulting.snam.rest.pojo.CompStatementHeader;
import com.adobe.consulting.snam.rest.pojo.SFOData;
import com.adobe.consulting.snam.rest.pojo.ServiceResult;
import com.adobe.consulting.snam.rest.pojo.SnamRestServiceProxy;
import com.adobe.consulting.snam.rest.pojo.UpsertResult;
import com.adobe.consulting.snam.rest.utils.IDPUtilities;
import com.adobe.consulting.snam.rest.utils.XmlUtilities;
import com.adobe.idp.Document;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;

public class SnamRestServiceImpl {

	protected boolean m_useStandardConnectionEngine;
	protected boolean m_verbose;
	protected String m_proxyHost;
	protected String m_proxyPort;
	protected String m_proxyUser;
	protected String m_proxyPassword;

	static Logger logger = Logger.getLogger(SnamRestServiceImpl.class);

	public ServiceResult callService(
			String serviceName, 
			ConnectionBean connectionBean,
			QueryBean queryBean) throws IOException {

		ResteasyClient client = createRestEasyClient();

		ResteasyWebTarget target = client.target(UriBuilder.fromPath(connectionBean.getUrl()));
		target.register(new BasicAuthentication(connectionBean.getUser(), connectionBean.getPassword()));

		SnamRestServiceProxy proxy = target.proxy(SnamRestServiceProxy.class);

		//CHECK FOR NULL FILTER/EXPAND/TOKEN)
		String szFormat = queryBean.getFormat();
		String szSelect = queryBean.getSelect();
		String szFilter = null;
		String szExpand = null;
		String szFromDate = null;
		String szToDate = null;
		String szSkipToken = null;

		if(queryBean.getFilter() != null && !queryBean.getFilter().isEmpty()) szFilter = queryBean.getFilter();
		if(queryBean.getExpand() != null && !queryBean.getExpand().isEmpty()) szExpand = queryBean.getExpand();
		if(queryBean.getFromDate() != null && !queryBean.getFromDate().isEmpty()) szFromDate = queryBean.getFromDate();
		if(queryBean.getToDate() != null && !queryBean.getToDate().isEmpty()) szToDate = queryBean.getToDate();
		if(queryBean.getSkiptoken() != null && !queryBean.getSkiptoken() .isEmpty()) szSkipToken = queryBean.getSkiptoken() ;

		if(m_verbose) logger.warn("callService rest parameters\n{\nformat: " + szFormat +
				"\nfilter: " + szFilter +
				"\nselect: " + szSelect +
				"\nexpand: " + szExpand +
				"\nfromDate: " + szFromDate +
				"\ntoDate: " + szToDate +
				"\nskiptoken: " + szSkipToken + "\n}");

		Response response = null;

		if(serviceName.equals("perPersonal")) {
			response = proxy.PerPersonal(szFormat, szFilter, szSelect, szExpand, szFromDate, szToDate, szSkipToken);
		}else if(serviceName.equals("empJob")) {
			response = proxy.EmpJob(szFormat, szFilter, szSelect, szExpand, szFromDate, szToDate, szSkipToken);
		}else if(serviceName.equals("empEmployment")) {
			response = proxy.EmpEmployment(szFormat, szFilter, szSelect, szExpand, szFromDate, szToDate, szSkipToken);
		}else if(serviceName.equals("empCompensation")) {
			response = proxy.EmpCompensation(szFormat, szFilter, szSelect, szExpand, szFromDate, szToDate, szSkipToken);
		}else if(serviceName.equals("empPayCompRecurring")) {
			response = proxy.EmpPayCompRecurring(szFormat, szFilter, szSelect, szExpand, szFromDate, szToDate, szSkipToken);
		}else if(serviceName.equals("empPayCompNonRecurring")) {
			response = proxy.EmpPayCompNonRecurring(szFormat, szFilter, szSelect, szExpand, szFromDate, szToDate, szSkipToken);

		}else {
			logger.error("ServiceName '" + serviceName + "' is not supported");
			return null;
		}

		String strJson = response.readEntity(String.class);

		ObjectMapper mapper = new ObjectMapper();

		ServiceResult serviceResult = mapper.readValue(strJson, ServiceResult.class);

		response.close();

		return serviceResult;

	}

	public ServiceResult empJob(String baseUrl, String user, String pwd, String format, String filter, String select, String expand, String fromDate, String toDate, String skipToken) throws IOException {

		ResteasyClient client = createRestEasyClient();

		ResteasyWebTarget target = client.target(UriBuilder.fromPath(baseUrl));
		target.register(new BasicAuthentication(user, pwd));

		SnamRestServiceProxy proxy = target.proxy(SnamRestServiceProxy.class);

		//CHECK FOR NULL FILTER/EXPAND/TOKEN)
		String szFilter = null;
		String szSkipToken = null;
		String szExpand = null;
		String szFromDate = null;
		String szToDate = null;

		if(filter != null && !filter.isEmpty()) szFilter = filter;
		if(expand != null && !expand.isEmpty()) szExpand = expand;
		if(fromDate != null && !fromDate.isEmpty()) szFromDate = fromDate;
		if(toDate != null && !toDate.isEmpty()) szToDate = toDate;
		if(skipToken != null && !skipToken.isEmpty()) szSkipToken = skipToken;

		Response response = proxy.EmpJob(format, szFilter, select, szExpand, szFromDate, szToDate, szSkipToken);

		String strJson = response.readEntity(String.class);

		ObjectMapper mapper = new ObjectMapper();

		ServiceResult serviceResult = mapper.readValue(strJson, ServiceResult.class);

		response.close();

		return serviceResult;

	}

	public ServiceResult empEmployment(String baseUrl, String user, String pwd, String format, String filter, String select, String expand, String fromDate, String toDate, String skipToken) throws IOException {

		ResteasyClient client = createRestEasyClient();

		ResteasyWebTarget target = client.target(UriBuilder.fromPath(baseUrl));
		target.register(new BasicAuthentication(user, pwd));

		SnamRestServiceProxy proxy = target.proxy(SnamRestServiceProxy.class);

		//CHECK FOR NULL FILTER/EXPAND/TOKEN)
		String szFilter = null;
		String szSkipToken = null;
		String szExpand = null;
		String szFromDate = null;
		String szToDate = null;

		if(filter != null && !filter.isEmpty()) szFilter = filter;
		if(expand != null && !expand.isEmpty()) szExpand = expand;
		if(fromDate != null && !fromDate.isEmpty()) szFromDate = fromDate;
		if(toDate != null && !toDate.isEmpty()) szToDate = toDate;
		if(skipToken != null && !skipToken.isEmpty()) szSkipToken = skipToken;

		Response response = proxy.EmpEmployment(format, szFilter, select, szExpand, szFromDate, szToDate, szSkipToken);

		String strJson = response.readEntity(String.class);

		ObjectMapper mapper = new ObjectMapper();

		ServiceResult serviceResult = mapper.readValue(strJson, ServiceResult.class);

		response.close();

		return serviceResult;

	}

	public ServiceResult perPersonal(String baseUrl, String user, String pwd, String format, String filter, String select, String expand, String fromDate, String toDate, String skipToken) throws IOException {

		ResteasyClient client = createRestEasyClient();

		ResteasyWebTarget target = client.target(UriBuilder.fromPath(baseUrl));
		target.register(new BasicAuthentication(user, pwd));

		SnamRestServiceProxy proxy = target.proxy(SnamRestServiceProxy.class);

		//CHECK FOR NULL FILTER/EXPAND/TOKEN)
		String szFilter = null;
		String szSkipToken = null;
		String szExpand = null;
		String szFromDate = null;
		String szToDate = null;

		if(filter != null && !filter.isEmpty()) szFilter = filter;
		if(expand != null && !expand.isEmpty()) szExpand = expand;
		if(fromDate != null && !fromDate.isEmpty()) szFromDate = fromDate;
		if(toDate != null && !toDate.isEmpty()) szToDate = toDate;
		if(skipToken != null && !skipToken.isEmpty()) szSkipToken = skipToken;

		Response response = proxy.PerPersonal(format, szFilter, select, szExpand, szFromDate, szToDate, szSkipToken);

		String strJson = response.readEntity(String.class);

		ObjectMapper mapper = new ObjectMapper();

		ServiceResult serviceResult = mapper.readValue(strJson, ServiceResult.class);

		response.close();

		return serviceResult;

	}

	public ServiceResult empCompensation(String baseUrl, String user, String pwd, String format, String filter, String select, String expand, String fromDate, String toDate, String skipToken) throws IOException {

		ResteasyClient client = createRestEasyClient();

		ResteasyWebTarget target = client.target(UriBuilder.fromPath(baseUrl));
		target.register(new BasicAuthentication(user, pwd));

		SnamRestServiceProxy proxy = target.proxy(SnamRestServiceProxy.class);

		//CHECK FOR NULL FILTER/EXPAND/TOKEN)
		String szFilter = null;
		String szSkipToken = null;
		String szExpand = null;
		String szFromDate = null;
		String szToDate = null;

		if(filter != null && !filter.isEmpty()) szFilter = filter;
		if(expand != null && !expand.isEmpty()) szExpand = expand;
		if(fromDate != null && !fromDate.isEmpty()) szFromDate = fromDate;
		if(toDate != null && !toDate.isEmpty()) szToDate = toDate;
		if(skipToken != null && !skipToken.isEmpty()) szSkipToken = skipToken;

		Response response = proxy.EmpCompensation(format, szFilter, select, szExpand, szFromDate, szToDate, szSkipToken);

		String strJson = response.readEntity(String.class);

		ObjectMapper mapper = new ObjectMapper();

		ServiceResult serviceResult = mapper.readValue(strJson, ServiceResult.class);

		response.close();

		return serviceResult;

	}

	public ServiceResult empPayCompRecurring(String baseUrl, String user, String pwd, String format, String filter, String select, String expand, String fromDate, String toDate, String skipToken) throws IOException {

		ResteasyClient client = createRestEasyClient();

		ResteasyWebTarget target = client.target(UriBuilder.fromPath(baseUrl));
		target.register(new BasicAuthentication(user, pwd));

		SnamRestServiceProxy proxy = target.proxy(SnamRestServiceProxy.class);

		//CHECK FOR NULL FILTER/EXPAND/TOKEN)
		String szFilter = null;
		String szSkipToken = null;
		String szExpand = null;
		String szFromDate = null;
		String szToDate = null;

		if(filter != null && !filter.isEmpty()) szFilter = filter;
		if(expand != null && !expand.isEmpty()) szExpand = expand;
		if(fromDate != null && !fromDate.isEmpty()) szFromDate = fromDate;
		if(toDate != null && !toDate.isEmpty()) szToDate = toDate;
		if(skipToken != null && !skipToken.isEmpty()) szSkipToken = skipToken;

		Response response = proxy.EmpPayCompRecurring(format, szFilter, select, szExpand, szFromDate, szToDate, szSkipToken);

		String strJson = response.readEntity(String.class);

		ObjectMapper mapper = new ObjectMapper();

		ServiceResult serviceResult = mapper.readValue(strJson, ServiceResult.class);

		response.close();

		return serviceResult;

	}

	public ServiceResult empPayCompNonRecurring(String baseUrl, String user, String pwd, String format, String filter, String select, String expand, String fromDate, String toDate, String skipToken) throws IOException {

		ResteasyClient client = createRestEasyClient();

		ResteasyWebTarget target = client.target(UriBuilder.fromPath(baseUrl));
		target.register(new BasicAuthentication(user, pwd));

		SnamRestServiceProxy proxy = target.proxy(SnamRestServiceProxy.class);

		//CHECK FOR NULL FILTER/EXPAND/TOKEN)
		String szFilter = null;
		String szSkipToken = null;
		String szExpand = null;
		String szFromDate = null;
		String szToDate = null;

		if(filter != null && !filter.isEmpty()) szFilter = filter;
		if(expand != null && !expand.isEmpty()) szExpand = expand;
		if(fromDate != null && !fromDate.isEmpty()) szFromDate = fromDate;
		if(toDate != null && !toDate.isEmpty()) szToDate = toDate;
		if(skipToken != null && !skipToken.isEmpty()) szSkipToken = skipToken;

		Response response = proxy.EmpPayCompNonRecurring(format, szFilter, select, szExpand, szFromDate, szToDate, szSkipToken);

		String strJson = response.readEntity(String.class);

		ObjectMapper mapper = new ObjectMapper();

		ServiceResult serviceResult = mapper.readValue(strJson, ServiceResult.class);

		response.close();

		return serviceResult;

	}

	public UpsertResult upsertPhase1(
			String baseUrl, 
			String user, 
			String pwd, 
			CompStatementHeader compStatementHeader) throws IOException, ParserConfigurationException, SAXException, XPathExpressionException {

		Response response;
		UpsertResult upsertResult;

		ResteasyClient client = createRestEasyClient();

		ResteasyWebTarget target = client.target(UriBuilder.fromPath(baseUrl));
		target.register(new BasicAuthentication(user, pwd));

		SnamRestServiceProxy proxy = target.proxy(SnamRestServiceProxy.class);

		String jsonObj = obj2Json(compStatementHeader);

		if(m_verbose) logger.warn("CompStatementHeader json:\n" + jsonObj);

		response = proxy.UpsertPhase1(jsonObj);

		String strJson = response.readEntity(String.class);

		ObjectMapper mapper = new ObjectMapper();

		upsertResult = mapper.readValue(strJson, UpsertResult.class);

		response.close();

		return upsertResult;
	}

	public UpsertResult upsertPhase2(
			String baseUrl, 
			String user, 
			String pwd, 
			Attachment attachment) throws JsonParseException, JsonMappingException, IOException {

		Response response;
		UpsertResult upsertResult;

		ResteasyClient client = createRestEasyClient();

		ResteasyWebTarget target = client.target(UriBuilder.fromPath(baseUrl));
		target.register(new BasicAuthentication(user, pwd));

		SnamRestServiceProxy proxy = target.proxy(SnamRestServiceProxy.class);

		String jsonAttachment = obj2Json(attachment);

		if(m_verbose) logger.warn("Attachment json:\n" + jsonAttachment);

		response = proxy.UpsertPhase2(jsonAttachment);

		String strJson = response.readEntity(String.class);

		ObjectMapper mapper = new ObjectMapper();

		upsertResult = mapper.readValue(strJson, UpsertResult.class);

		response.close();

		return upsertResult;

	}
	public UpsertResult upsertPhase3(
			String baseUrl, 
			String user, 
			String pwd, 
			CompStatementDetailsNav compStatementDetailsNav) throws JsonParseException, JsonMappingException, IOException {

		Response response;
		UpsertResult upsertResult;

		ResteasyClient client = createRestEasyClient();

		ResteasyWebTarget target = client.target(UriBuilder.fromPath(baseUrl));
		target.register(new BasicAuthentication(user, pwd));

		SnamRestServiceProxy proxy = target.proxy(SnamRestServiceProxy.class);

		String jsonCompStatementDetailsNav = obj2Json(compStatementDetailsNav);

		if(m_verbose) logger.warn("compStatementDetailsNav json:\n" + jsonCompStatementDetailsNav);

		response = proxy.UpsertPhase3(jsonCompStatementDetailsNav);

		String strJson = response.readEntity(String.class);

		ObjectMapper mapper = new ObjectMapper();

		upsertResult = mapper.readValue(strJson, UpsertResult.class);

		response.close();

		return upsertResult;
	}

	public int persistSFODataEntities(
			ConnectionBean connectionBean, 
			org.w3c.dom.Document xmlJob, 
			List<SFOData> sfoDataEntities,
			boolean skipExisting, 
			boolean updateExisting, 
			int entityStatementIndex,
			boolean dumpXmlJob,
			boolean dumpXmlOperations,
			boolean dumpXmlSfo,
			boolean dumpXmlData) throws Exception {

		DataSource ds = null;
		Connection con = null; 
		InitialContext ic = null; 
		PreparedStatement pr = null;

		int processed = 0;

		try {

			if(xmlJob == null) {
				logger.warn("xmlJob is null, cannot continue");
				return -1;
			}

			if(sfoDataEntities == null) {
				logger.warn("sfoDataEntities is null, no entities to insert, cannot continue");
				return -1;
			}

			//Class.forName("oracle.jdbc.driver.OracleDriver");
			//con = DriverManager.getConnection("jdbc:oracle:thin:@//colj2oraferab-scan.risorse.int:1521/CFERTER","LIVECYCLE_COL","LIVECYCLE_COL");

			//con = DriverManager.getConnection("jdbc:mysql://aem.pico.it:3306/adobe?useSSL=false", "adobe", "adobe");

			ic = new InitialContext();
			ds = (DataSource)ic.lookup(connectionBean.getJndi());
			con = ds.getConnection();

			//DUMP JOB
			if(dumpXmlJob) {
				logger.warn(XmlUtilities.DOMToString(xmlJob, 4));
			}

			String jobType = XmlUtilities.executeXPathString(xmlJob, "/job/@type");
			String templatePath = XmlUtilities.executeXPathString(xmlJob, "/job/@templatePath");

			String statement_exist = XmlUtilities.executeXPathString(xmlJob, "/job/persistence/statement_exist");
			String statement_persist = XmlUtilities.executeXPathString(xmlJob, "/job/persistence/statement_persist");
			String statement_update = XmlUtilities.executeXPathString(xmlJob, "/job/persistence/statement_update");

			org.w3c.dom.Document xmlExternalData = XmlUtilities.newXmlFromXPath(xmlJob, "/job/externalData");

			String external_data = XmlUtilities.DOMToString(xmlExternalData, 4);

			if(m_verbose)logger.warn("extended_data str:\n" + external_data);

			ExternalData externalData = (ExternalData) XmlUtilities.deserializeObject(external_data, ExternalData.class);

			String strExternalData = XmlUtilities.serializeObject(externalData);

			if(m_verbose)logger.warn("strExternalData:\n" + strExternalData);

			if(m_verbose) {
				if(externalData.getSources().size() == 0) {
					logger.warn("externalDataSources size is 0");
				}else {
					logger.info("externalDataSources size is " + externalData.getSources().size());
				}
			}

			org.w3c.dom.Document xmlOperations = XmlUtilities.newXmlFromXPath(xmlJob, "//operation", "operations");

			//DUMP OPERATIONS
			if(dumpXmlOperations) {
				logger.warn(XmlUtilities.DOMToString(xmlOperations, 4));
			}

			List<OperationNodeMerge> mergeOperations = generateOperations(
					"merge",
					xmlOperations,
					"//operation[@type='merge']",
					"target",
					"destination",
					"rename",
					"exclude",
					"function",
					"functionParameters");

			if(m_verbose) {
				if(mergeOperations.size() == 0) {
					logger.warn("mergeOperations size is " + mergeOperations.size());
				}else {
					logger.info("mergeOperations size is " + mergeOperations.size());
				}
			}

			List<OperationNodeRename> renameOperations = generateRenameOperations(
					xmlOperations,
					"//operation[@type='rename']",
					"target",
					"oldName",
					"newName");

			if(m_verbose) {
				if(renameOperations.size() == 0) {
					logger.warn("renameOperations size is " + renameOperations.size());
				}else {
					logger.info("renameOperations size is " + renameOperations.size());
				}
			}

			List<OperationNodeMerge> modifyOperations = generateOperations(
					"modify",
					xmlOperations,
					"//operation[@type='modify']",
					"target",
					"destination",
					"rename",
					"exclude",
					"function",
					"functionParameters");

			if(m_verbose) {
				if(renameOperations.size() == 0) {
					logger.warn("modifyOperations size is " + modifyOperations.size());
				}else {
					logger.info("modifyOperations size is " + modifyOperations.size());
				}
			}

			if(skipExisting) {
				if(statement_exist == null) {
					logger.error("skipExisting is true but statement_exist is null cannot continue");
					return -1;
				}

				if(m_verbose) logger.info("Skip existing flag is enabled");
			}

			if(updateExisting) {
				if(statement_update == null) {
					logger.error("updateExisting is true but statement_update is null cannot continue");
					return -1;
				}

				if(m_verbose) logger.info("Update existing flag is enabled");
			}

			for (SFOData sfoData : sfoDataEntities) {

				boolean exists = false;
				boolean execute = false;

				String userId = null;
				String personIdExternal = null;
				String formalName = null;

				//SERIALIZE SFO OBJECT
				org.w3c.dom.Document xmlSFO = obj2XmlDocument(sfoData);

				//DUMP JOB
				if(dumpXmlSfo) {
					logger.warn(XmlUtilities.DOMToString(xmlSFO, 4));
				}

				logger.warn("Executing MERGE operations");
				//EXECUTING MERGE OPERATIONS
				org.w3c.dom.Document xmlData = mergeMultiNodesXmlMultiXPathsEx(xmlSFO, null, mergeOperations, "DATI", m_verbose);

				logger.warn("Executing RENAME operations");
				//EXECUTING RENAME OPERATIONS
				xmlData = renameXmlNodes(xmlData, renameOperations, m_verbose);

				//EXECUTING MODIFY OPERATIONS
				logger.warn("Executing MODIFY operations");
				xmlData = mergeMultiNodesXmlMultiXPathsEx(xmlData, xmlData, modifyOperations, "DATI", m_verbose);

				//DUMP JOB
				if(dumpXmlJob) {
					logger.warn(XmlUtilities.DOMToString(xmlData, 4));
				}

				userId = XmlUtilities.executeXPathString(xmlData, "/DATI/userId");
				personIdExternal = XmlUtilities.executeXPathString(xmlData, "/DATI/personIdExternal");
				formalName = XmlUtilities.executeXPathString(xmlData, "/DATI/formalName");

				//EXTERNAL DATA
				for (ExternalDataSource externalDataSource : externalData.getSources()) {

					org.w3c.dom.Document xmlExternalDataSource = obj2XmlDocument(externalDataSource);
					logger.warn("externalDataSource\n" + XmlUtilities.DOMToString(xmlExternalDataSource, 4));

					ExternalDataSource externalDataSourceCloned = externalDataSource.clone();

					if(externalDataSourceCloned.isEnabled()) {

						//EXECUTE ALL XPATHS FOR PARAMETERS
						for (QueryParameterBean queryParameterBean : externalDataSourceCloned.getQuery().getParameters()) {

							if(queryParameterBean.getTarget() != null && !queryParameterBean.getTarget().isEmpty()) {

								if(m_verbose) logger.info("Executing xpath for externalDataParameter - target: " + queryParameterBean.getTarget());

								//RESOLVE XPATH
								String value = XmlUtilities.executeXPathString(xmlData, queryParameterBean.getTarget());

								if(queryParameterBean.getModify() != null && !queryParameterBean.getModify().trim().isEmpty()) {

									//DATE
									if(queryParameterBean.getType().equals("date")) {

										if(m_verbose) logger.info("Executing modify for externalDataParameter(date) - modify: " + queryParameterBean.getModify());

										String[] args = queryParameterBean.getModify().split(",");

										Date date = epochDateToDate(value);
										LocalDateTime localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
										LocalDateTime modifiedDate = null;

										if(args[0].equals("+")) {
											modifiedDate = localDate.plusDays(Long.parseLong(args[1]));

										}else if(args[0].equals("-")) {
											modifiedDate = localDate.minusDays(Long.parseLong(args[1]));
										}

										String formattedDate = modifiedDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss"));

										if(m_verbose) logger.info("Executing modify for externalDataParameter(date) - formatted date: " + formattedDate);

										queryParameterBean.setValue(formattedDate);
									}

								}else {
									queryParameterBean.setValue(value);
								}
							}
						}

						if(externalDataSourceCloned.getType().equals("sql")) {

							if(m_verbose) logger.info("Calling externalDataSource(sql) - unprepared statement: " + externalDataSourceCloned.getQuery().getSelect());

							org.w3c.dom.Document xmlQueriedData = executeSQLQueryEx(connectionBean.getJndi(), externalDataSourceCloned.getQuery().getSelect(), externalDataSourceCloned.getQuery().getParameters());

							logger.warn("SQL DATA:" + XmlUtilities.DOMToString(xmlQueriedData, 4));

							xmlData = mergeMultiNodesXmlMultiXPathsEx(xmlQueriedData, xmlData, externalDataSourceCloned.getOperations(), "DATI", true);

							if(m_verbose) logger.info("Called externalDataSource(sql), merged Data:");

							logger.warn(XmlUtilities.DOMToString(xmlData, 4));

						} else if(externalDataSourceCloned.getType().equals("service")) {

							if(m_verbose) logger.info("Calling externalDataSource(service) - service " + externalDataSourceCloned.getService() + ", unprepared filter: " + externalDataSourceCloned.getQuery().getFilter());

							StatementHolder statementHolder = new StatementHolder(externalDataSourceCloned.getQuery().getFilter());

							for (QueryParameterBean queryParameterBean : externalDataSourceCloned.getQuery().getParameters()) {

								statementHolder.setArgument(queryParameterBean.getIndex(), queryParameterBean.getValue());

							}

							if(m_verbose) logger.info("Calling externalDataSource(service) - service: " + externalDataSourceCloned.getService() + ", prepared filter: " + statementHolder.getCompletedStatement());

							externalDataSourceCloned.getQuery().setFilter(statementHolder.getCompletedStatement());

							ServiceResult result = callService(
									externalDataSourceCloned.getService(),  
									connectionBean,
									externalDataSourceCloned.getQuery());

							if (result.getError() != null) {
								//ERROR
								throw new Exception("Errore callService " + result.getError().getMessage().getValue());

							}else {

								//SERIALIZE SFO OBJECT
								org.w3c.dom.Document response = obj2XmlDocument(result.getResponse());

								//DUMP JOB
								if(dumpXmlSfo) {
									logger.warn(XmlUtilities.DOMToString(response, 4));
								}

								if(result.getResponse().getRecords().size() > 0) {

									SFOData sfoDataExternal = result.getResponse().getRecords().get(0);

									org.w3c.dom.Document xmlQueriedData = obj2XmlDocument(sfoDataExternal);

									xmlData = mergeMultiNodesXmlMultiXPathsEx(xmlQueriedData, xmlData, externalDataSourceCloned.getOperations(), "DATI", true);

									if(m_verbose) logger.info("Called externalDataSource(service), merged Data:");

									logger.warn(XmlUtilities.DOMToString(xmlData, 4));

								}else {

									if(m_verbose) logger.warn("Called externalDataSource(service), no records returned");
								}

							}
						}
					}
				}

				if(skipExisting || updateExisting) {
					pr = con.prepareStatement(statement_exist);
					pr.setString(1, personIdExternal);
					pr.setString(2, jobType);

					ResultSet rs = pr.executeQuery();

					if(rs.next()) {
						if(rs.getInt(1) == 1) {
							exists = true;
							if(m_verbose) logger.warn("Found existing record with personIdExternal = '" +  personIdExternal + "'");
						}else if(rs.getInt(1) > 1) {
							exists = true;
							logger.error("Found " + rs.getInt(1) + " duplicated records with personIdExternal = '" +  personIdExternal + "'");
						}else if(rs.getInt(1) == 0) {
							if(m_verbose) logger.info("No existing record found with personIdExternal = '" +  personIdExternal + "'");
						}
					}

					rs.close();
					pr.close();
				}

				if(exists) {

					if(skipExisting) {
						//SALTA
						if(m_verbose) logger.info("Skip duplicate insertion of record with personIdExternal = '" +  personIdExternal + "'");
					}
					if(updateExisting) {
						pr = con.prepareStatement(statement_update);
						pr.setString(2, personIdExternal);
						execute = true;

						if(m_verbose) logger.info("Will update record with personIdExternal = '" +  personIdExternal + "'");
					}

				}else {
					execute = true;
					pr = con.prepareStatement(statement_persist);
					pr.setString(2, userId);
					pr.setString(3, personIdExternal);
					pr.setString(4, formalName);
					pr.setString(5, jobType);
					pr.setString(6, templatePath);
					if(m_verbose) logger.info("Prepared persiting statement for personIdExternal = '" +  personIdExternal + "'");
				}

				if(execute) {

					if(updateExisting && exists) {
						if(m_verbose) logger.info("Executing updating statement for personIdExternal = '" +  personIdExternal + "'");
					}else {
						if(m_verbose) logger.info("Executing persiting statement for personIdExternal = '" +  personIdExternal + "'");
					}

					String strNewDom = XmlUtilities.DOMToString(xmlData, 4);

					BufferedReader br = new BufferedReader(new StringReader(strNewDom));

					pr.setClob(entityStatementIndex, br);

					pr.executeUpdate();

					pr.close();

					processed++;

					if(updateExisting && exists) {
						if(m_verbose) logger.info("Successfully executed updating statement for sfo_id = '" +  personIdExternal + "'");
					}else {
						if(m_verbose) logger.info("Successfully executed persiting statement for sfo_id = '" +  personIdExternal + "'");
					}
				}
			}

			return processed;

		}catch(SQLException e){
			e.printStackTrace();
			throw e;
		}catch(UnsupportedOperationException e){
			e.printStackTrace();
			throw e;
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
			throw e;
		} catch (XPathExpressionException e) {
			e.printStackTrace();
			throw e;
		} catch (SAXException e) {
			e.printStackTrace();
			throw e;
		} catch (IOException e) {
			e.printStackTrace();
			throw e;
		}finally{
			if(con != null && !con.isClosed()) con.close();
			if(pr != null && !pr.isClosed()) pr.close();
		}

	} 

	public org.w3c.dom.Document executeSQLQuery(String jndiConnection, String statement) throws NamingException, SQLException, UnsupportedOperationException, ParserConfigurationException {

		DataSource ds = null;
		Connection con = null; 
		InitialContext ic = null; 
		PreparedStatement pr = null;

		try {

			//Class.forName("com.mysql.jdbc.Driver");
			//con = DriverManager.getConnection("jdbc:mysql://192.168.22.3:3306/adobe?useSSL=false", "adobe", "adobe");

			ic = new InitialContext();
			ds = (DataSource)ic.lookup(jndiConnection);
			con = ds.getConnection();
			pr = con.prepareStatement(statement);

			ResultSet rs = pr.executeQuery();

			Sql2XmlHelper helper = new Sql2XmlHelper();

			org.w3c.dom.Document xmlDocument = helper.writeXmlDocument(rs);

			return xmlDocument;

		}catch(SQLException e){
			e.printStackTrace();
			throw e;
		}catch(UnsupportedOperationException e){
			e.printStackTrace();
			throw e;
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
			throw e;
		}finally{
			if(con != null && !con.isClosed()) con.close();
			if(pr != null && !pr.isClosed()) pr.close();
		}
	} 

	public org.w3c.dom.Document executeSQLQueryEx(String jndiConnection, String statement, List<QueryParameterBean> parameters) throws NamingException, SQLException, UnsupportedOperationException, ParserConfigurationException {

		DataSource ds = null;
		Connection con = null; 
		InitialContext ic = null; 
		PreparedStatement pr = null;

		try {

			//Class.forName("com.mysql.jdbc.Driver");
			//con = DriverManager.getConnection("jdbc:mysql://192.168.22.3:3306/adobe?useSSL=false", "adobe", "adobe");

			ic = new InitialContext();
			ds = (DataSource)ic.lookup(jndiConnection);
			con = ds.getConnection();
			pr = con.prepareStatement(statement);

			for (QueryParameterBean queryParameterBean : parameters) {

				if(queryParameterBean.getType().equals("string")) {
					pr.setString(queryParameterBean.getIndex(), queryParameterBean.getValue());
				}else if(queryParameterBean.getType().equals("number")) {
					pr.setInt(queryParameterBean.getIndex(), Integer.parseInt(queryParameterBean.getValue()));
				}

			}

			ResultSet rs = pr.executeQuery();

			Sql2XmlHelper helper = new Sql2XmlHelper();

			org.w3c.dom.Document xmlDocument = helper.writeXmlDocument(rs);

			return xmlDocument;

		}catch(SQLException e){
			e.printStackTrace();
			throw e;
		}catch(UnsupportedOperationException e){
			e.printStackTrace();
			throw e;
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
			throw e;
		}finally{
			if(con != null && !con.isClosed()) con.close();
			if(pr != null && !pr.isClosed()) pr.close();
		}

	} 

	public int executeSQLUpdate(String jndiConnection, String statement) throws NamingException, SQLException, UnsupportedOperationException {

		DataSource ds = null;
		Connection con = null; 
		InitialContext ic = null; 
		PreparedStatement pr = null;
		try {

			//Class.forName("oracle.jdbc.driver.OracleDriver");
			//con = DriverManager.getConnection("jdbc:oracle:thin:@//colj2oraferab-scan.risorse.int:1521/CFERTER","LIVECYCLE_COL","LIVECYCLE_COL");

			//con = DriverManager.getConnection("jdbc:mysql://aem.pico.it:3306/adobe?useSSL=false", "adobe", "adobe");

			ic = new InitialContext();
			ds = (DataSource)ic.lookup(jndiConnection);
			con = ds.getConnection();

			pr = con.prepareStatement(statement);

			int result = pr.executeUpdate();

			return result;

		}catch(SQLException e){
			e.printStackTrace();
			throw e;
		}catch(UnsupportedOperationException e){
			e.printStackTrace();
			throw e;
		}finally{
			if(con != null && !con.isClosed()) con.close();
			if(pr != null && !pr.isClosed()) pr.close();
		}
	} 

	public boolean executeSQLUpdateBloB(String jndiConnection, String statement, com.adobe.idp.Document blob, int blobIndex) throws Exception{

		boolean result = false;
		PreparedStatement queryStatement = null;
		Connection connection = null;

		try
		{
			//String url = "jdbc:oracle:thin:@(DESCRIPTION=(LOAD_BALANCE=on)(ADDRESS=(PROTOCOL=TCP)(HOST=predberp12a-vip) (PORT=1521))(ADDRESS=(PROTOCOL=TCP)(HOST=predberp12b-vip) (PORT=1521))(CONNECT_DATA=(SERVICE_NAME=PREPROD)))";
			InitialContext context = new InitialContext();
			connection = ((DataSource)context.lookup(jndiConnection)).getConnection();

			queryStatement = connection.prepareStatement(statement);
			queryStatement.setBinaryStream(blobIndex, blob.getInputStream(), (int)blob.length());

			int results = queryStatement.executeUpdate();

			if (results == 1) {
				result = true;

			}else if (results == 0){
				logger.info("Successfully inserted blob");

			}else if (results > 1){
				logger.warn("Successfully inserted blob but too many records updated");
			}

		}catch (Exception e) {
			e.printStackTrace();
			throw e;

		}finally{

			if (connection != null){
				try {
					connection.close();
				} catch (Exception ex) {

				}
			}
			if (queryStatement != null) {
				try {
					queryStatement.close();
				} catch (Exception ex) {

				}
			}

		}

		return result;

	}

	public Document readBinary(String jndiConnection, String statement, int binaryIndex) throws Exception {
		Document binary = null;
		PreparedStatement queryStatement = null;
		Connection connection = null;
		java.sql.Blob binBlob = null;

		try{

			InitialContext context = new InitialContext();

			connection = ((DataSource)context.lookup(jndiConnection)).getConnection();

			queryStatement = connection.prepareStatement(statement);

			ResultSet results = queryStatement.executeQuery();
			results.next();
			binBlob = results.getBlob(binaryIndex);

			//			System.out.println("LEN: " + binBlob.length());
			//			
			//			byte[] data = new byte[(int) binBlob.length()];
			//			
			//			data = binBlob.getBytes(1L, (int) binBlob.length());
			//			
			//			System.out.println("EFF LEN: " + data.length);

			byte[] imgAsBytes = binBlob.getBytes(1, (int) binBlob.length());
			
			binary = new Document(imgAsBytes);

		} catch (Exception e) {

			e.printStackTrace();
			throw e;

		}finally{

			if (connection != null){
				try {
					connection.close();
				} catch (Exception ex) {

				}
			}
			if (queryStatement != null) {
				try {
					queryStatement.close();
				} catch (Exception ex) {

				}
			}

		}

		return binary;
	}

	public List<OperationNodeMerge> generateOperations(
			String type,
			org.w3c.dom.Document xmlOperations, 
			String extractionXPath, 
			String sourceNodeAttribute,
			String targetNodeAttribute,
			String targetNodeRenameAttribute,
			String excludedNodesAttribute,
			String functionAttribute,
			String functionParametersAttribute) throws Exception {

		List<OperationNodeMerge> operations = new ArrayList<OperationNodeMerge>();

		NodeList nl = XmlUtilities.executeXPathNodes(xmlOperations, extractionXPath);

		for (int i = 0; i < nl.getLength(); i++) {

			Node node = nl.item(i);

			Element ele = (Element) node;

			operations.add(new OperationNodeMerge(type,
					ele.getAttribute(sourceNodeAttribute), 
					ele.getAttribute(targetNodeAttribute), 
					ele.getAttribute(targetNodeRenameAttribute),
					ele.getAttribute(excludedNodesAttribute),
					ele.getAttribute(functionAttribute),
					ele.getAttribute(functionParametersAttribute)));
		}

		return operations;
	}

	public List<OperationNodeRename> generateRenameOperations(
			org.w3c.dom.Document xmlOperations, 
			String extractionXPath, 
			String nodeXPath, 
			String nodeNameAttribute,
			String newNodeNameAttribute) throws Exception {

		List<OperationNodeRename> operations = new ArrayList<OperationNodeRename>();

		NodeList nl = XmlUtilities.executeXPathNodes(xmlOperations, extractionXPath);

		for (int i = 0; i < nl.getLength(); i++) {

			Node node = nl.item(i);

			Element ele = (Element) node;

			operations.add(new OperationNodeRename(
					ele.getAttribute(nodeXPath), 
					ele.getAttribute(nodeNameAttribute), 
					ele.getAttribute(newNodeNameAttribute)));
		}

		return operations;
	}

	public ConnectionBean deserializeConnectionBean(org.w3c.dom.Document xmlConnectionBean) throws XPathExpressionException, ParserConfigurationException, SAXException, IOException, ClassNotFoundException, JAXBException {

		String str = XmlUtilities.DOMToString(xmlConnectionBean, 4);

		return (ConnectionBean) XmlUtilities.deserializeObject(str, ConnectionBean.class);
	}

	public Object deserializeObject(org.w3c.dom.Document objectXml, String className) throws XPathExpressionException, ParserConfigurationException, SAXException, IOException, ClassNotFoundException, JAXBException {

		String str = XmlUtilities.DOMToString(objectXml, 4);

		return XmlUtilities.deserializeObject(str, Class.forName(className));
	}

	public String obj2XmlString(Object object) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {

		XStream xstream = new XStream(new StaxDriver());

		return xstream.toXML(object);

	}

	public org.w3c.dom.Document obj2XmlDocument(Object object) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {

		XStream xstream = new XStream(new StaxDriver());

		DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		org.w3c.dom.Document document = db.parse(new ByteArrayInputStream(xstream.toXML(object).getBytes("UTF-8")));

		return document;

	}

	public Object xml2Obj(String xml) {

		XStream xstream = new XStream(new StaxDriver());

		xstream.allowTypesByWildcard(new String[] {
				"com.adobe.**", "org.apache.**", "java.**"
		});

		return xstream.fromXML(xml);
	}	

	public String obj2Json(Object object) {
		Gson gson = new GsonBuilder().create();
		String json = gson.toJson(object);// obj is your object 

		return json;
	}

	public Object json2Obj(String json, String className) throws JsonSyntaxException, ClassNotFoundException {
		Gson gson = new GsonBuilder().create();
		Object obj2 = gson.fromJson(json, Class.forName(className));

		return obj2;   
	}

	public String document2Base64(com.adobe.idp.Document doc) throws IOException{
		return IDPUtilities.document2Base64(doc);
	}

	public Document base642Document(String base64, String contentType) throws IOException{
		return IDPUtilities.base642Document(base64, contentType);
	}

	public org.w3c.dom.Document mergeMultiNodesXmlMultiXPathsEx(
			org.w3c.dom.Document sourceDoc, 
			org.w3c.dom.Document destinationDoc, 
			List<OperationNodeMerge> mergeOperations, 
			String defaultRootName,
			boolean verbose) throws Exception{

		DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
		domFactory.setNamespaceAware(true); 

		//create path
		//checkCreateNodePath(destinationDoc, destinationXPath);

		if(destinationDoc == null) {

			if (verbose) logger.info("mergeMultiNodesXMLMultiXPathsEx: destinationDoc parameter is null/empty and will be initialized");

			destinationDoc = domFactory.newDocumentBuilder().newDocument();

			Element rootElement = null;

			if(defaultRootName == null) {
				defaultRootName = "root";
			}

			if (verbose) logger.info("mergeMultiNodesXMLMultiXPathsEx: destinationDoc initialized with root node named " + defaultRootName);

			rootElement = destinationDoc.createElement(defaultRootName);

			destinationDoc.appendChild(rootElement);
		}

		if (verbose) logger.info("mergeMultiNodesXMLMultiXPathsEx: xPathsMap has " + mergeOperations.size() + " elements");


		for (OperationNodeMerge operationXPath : mergeOperations) {

			if (verbose) logger.info("mergeMultiNodesXMLMultiXPathsEx: xPath element - target: " + operationXPath.getTarget() + ", destination: " + operationXPath.getDestination() + ", rename: " + operationXPath.getRename());
		}

		if (verbose) logger.info("mergeMultiNodesXMLMultiXPathsEx: starting merging operations");

		for (OperationNodeMerge operationXPath : mergeOperations) {

			NodeList ns = ((NodeList)XmlUtilities.getNodeList(operationXPath.getTarget(), sourceDoc));

			if (verbose) logger.info("mergeMultiNodesXMLMultiXPathsEx: creating nodes path if needed (" + operationXPath.getDestination() + ")");

			destinationDoc = XmlUtilities.checkCreateNodePath(destinationDoc, operationXPath.getDestination());

			NodeList nd  = ((NodeList)XmlUtilities.getNodeList(operationXPath.getDestination(), destinationDoc));

			if (ns!=null && nd!=null) 
			{		
				for (int i = 0; i < nd.getLength(); i++) {
					Node node = nd.item(i);

					for (int j = 0; j < ns.getLength(); j++) {

						Node sourceNode = ns.item(j);

						if(operationXPath.getExclude() != null && !operationXPath.getExclude().isEmpty()) {

							String[] excludeNodeXpaths = operationXPath.getExclude().split(",");

							for (String excludeNodeXpath : excludeNodeXpaths) {

								NodeList rn  = ((NodeList)XmlUtilities.getNodeList(excludeNodeXpath, sourceNode));

								for (int k = 0; k < rn.getLength(); k++) {
									sourceNode.removeChild(rn.item(k));
								}
							}

						}

						Node newNode = destinationDoc.importNode(sourceNode, true);

						if(operationXPath.getRename() != null && !operationXPath.getRename().isEmpty()) {
							destinationDoc.renameNode(newNode, null, operationXPath.getRename());
						}

						if(operationXPath.getFunction() != null && !operationXPath.getFunction().isEmpty()) {

							if(newNode.getTextContent() != null && !newNode.getTextContent().isEmpty()) {

								if(operationXPath.getFunction().equals("substring")) {

									int endIndex = 0;

									String[] params = operationXPath.getFunctionParameters().split(",");

									if(params.length == 1) {
										endIndex = (newNode.getTextContent().length() - 1);
									}else {
										endIndex = Integer.parseInt(params[1]);
									}

									if (verbose) logger.info("mergeMultiNodesXMLMultiXPathsEx: executing substring function, node " + newNode.getNodeName() + ", value " + newNode.getTextContent() + ", startIndex: " + params[0] + ", endIndex: " + endIndex);

									Method method = String.class.getMethod("substring", int.class, int.class);

									String modified = (String) method.invoke(newNode.getTextContent(), Integer.parseInt(params[0]), endIndex);

									newNode.setTextContent(modified);

									if (verbose) logger.info("mergeMultiNodesXMLMultiXPathsEx: executed substring function, node " + newNode.getNodeName() + ", new value " + modified);

								}

								if(operationXPath.getFunction().equals("replace")) {

									String replacement;

									String[] params = operationXPath.getFunctionParameters().split(",");

									if(params.length == 1) {
										replacement = "";
									}else {
										replacement = params[1];
									}

									if (verbose) logger.info("mergeMultiNodesXMLMultiXPathsEx: executing replace function, node " + newNode.getNodeName() + ", value " + newNode.getTextContent() + ", target: " + params[0] + ", replacement: " + replacement);

									Method method = String.class.getMethod("replace", CharSequence.class, CharSequence.class);

									String modified = (String) method.invoke(newNode.getTextContent(), params[0], params[1]);

									newNode.setTextContent(modified);

									if (verbose) logger.info("mergeMultiNodesXMLMultiXPathsEx: executed replace function, node " + newNode.getNodeName() + ", new value " + modified);

								}

								if(operationXPath.getFunction().equals("replaceAll")) {

									String replacement;

									String[] params = operationXPath.getFunctionParameters().split(",");

									if(params.length == 1) {
										replacement = "";
									}else {
										replacement = params[1];
									}

									if (verbose) logger.info("mergeMultiNodesXMLMultiXPathsEx: executing replaceAll function, node " + newNode.getNodeName() + ", value " + newNode.getTextContent() + ", regExp: " + params[0] + ", replacement: " + replacement);

									Method method = String.class.getMethod("replaceAll", String.class, String.class);

									String modified = (String) method.invoke(newNode.getTextContent(), params[0], replacement);

									newNode.setTextContent(modified);

									if (verbose) logger.info("mergeMultiNodesXMLMultiXPathsEx: executed replaceAll function, node " + newNode.getNodeName() + ", new value " + modified);

								}

								if(operationXPath.getFunction().equals("epochToDate")) {

									String strDate = epochDateToString(newNode.getTextContent(), operationXPath.getFunctionParameters());

									if (verbose) logger.info("mergeMultiNodesXMLMultiXPathsEx: executing epochToDate function, node " + newNode.getNodeName() + ", value " + newNode.getTextContent() + ", format: " + operationXPath.getFunctionParameters());

									newNode.setTextContent(strDate);

									if (verbose) logger.info("mergeMultiNodesXMLMultiXPathsEx: executed epochToDate function, node " + newNode.getNodeName() + ", new value " + strDate);

								}

								if(operationXPath.getFunction().equals("dateToEpoch")) {

									String strDate = dateStringToDateEpoch(newNode.getTextContent(), operationXPath.getFunctionParameters());

									if (verbose) logger.info("mergeMultiNodesXMLMultiXPathsEx: executing dateToEpoch function, node " + newNode.getNodeName() + ", value " + newNode.getTextContent() + ", format: " + operationXPath.getFunctionParameters());

									newNode.setTextContent(strDate);

									if (verbose) logger.info("mergeMultiNodesXMLMultiXPathsEx: executed dateToEpoch function, node " + newNode.getNodeName() + ", new value " + strDate);

								}
								//newNode.setNodeValue(XmlUtilities.executeXPathString(newNode, operationXPath.getxPathFunction()));

							}
						}

						node.appendChild(newNode);
					}
				}
			}

			String finalDestination;

			if(operationXPath.getRename() != null && !operationXPath.getRename().isEmpty()) {
				finalDestination = operationXPath.getRename();
			}else {
				finalDestination = operationXPath.getRename();
			}

			if (verbose) logger.info("mergeMultiNodesXMLMultiXPathsEx: " + ns.getLength() + " nodes(" + operationXPath.getTarget() + ") appended into " + nd.getLength() + " nodes(" + finalDestination + ")");
		}

		return destinationDoc;
	}		

	private org.w3c.dom.Document renameXmlNodes(
			org.w3c.dom.Document sourceDoc, 
			List<OperationNodeRename> operations, 
			boolean verbose) throws Exception{

		DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
		domFactory.setNamespaceAware(true); 

		//create path
		//checkCreateNodePath(destinationDoc, destinationXPath);
		if (verbose) logger.info("renameXmlNodes: Operations list has " + operations.size() + " elements");

		for (OperationNodeRename operation : operations) {

			if (verbose) logger.info("renameXmlNodes: Operation - Target: " + operation.getTarget() + ", OldName: " + operation.getOldName() + ", NewName: " + operation.getNewName());
		}

		if (verbose) logger.info("\renameXmlNodes: starting renaming operations\n");

		for (OperationNodeRename operation : operations) {

			int renamed = 0;

			NodeList ns = ((NodeList)XmlUtilities.getNodeList(operation.getTarget(), sourceDoc));

			if (ns!=null) 
			{		

				for (int j = 0; j < ns.getLength(); j++) {

					Node node = ns.item(j);

					if(node.getNodeName().equals(operation.getOldName())) {

						sourceDoc.renameNode(node, null, operation.getNewName());

						renamed++;
					}

				}
			}

			if (verbose) logger.info("renameXmlNodes: " + renamed + " nodes (" + operation.getOldName() + ") renamed to " + operation.getNewName());

		}

		return sourceDoc;
	}		

	private ResteasyClient createRestEasyClient() {

		ResteasyClient client = null;

		if(m_useStandardConnectionEngine) {	

			if (m_verbose) logger.warn("Using Standard Connection Engine");

			client = new ResteasyClientBuilder().httpEngine(new URLConnectionEngine()).build();

		}else {

			if (this.m_proxyHost != null) {

				if (m_verbose) logger.warn("Using Specified Proxy Settings");

				Credentials credentials = new UsernamePasswordCredentials(m_proxyUser, m_proxyPassword);
				CredentialsProvider credProvider = new BasicCredentialsProvider();
				credProvider.setCredentials(new AuthScope(m_proxyHost, Integer.parseInt(m_proxyPort)), credentials);

				HttpClient httpClient = HttpClientBuilder.create()
						.setProxy(new HttpHost(m_proxyHost, Integer.parseInt(m_proxyPort)))
						.setDefaultCredentialsProvider(credProvider)
						.setProxyAuthenticationStrategy(new ProxyAuthenticationStrategy())
						.build();
				ApacheHttpClient43Engine engine = new ApacheHttpClient43Engine(httpClient);

				client = new ResteasyClientBuilder()
						.httpEngine(engine)
						.build();

			}else {
				client = new ResteasyClientBuilder().build();
			}
		}

		return client;

	}

	private Date epochDateToDate(String epochDate) {

		String filteredDate = null;
		Date newDate = null;

		if(epochDate.contains("/Date")) {

			filteredDate = epochDate.replace("/Date(", "").replace(")/", "");

		} else {

			filteredDate = epochDate;
		}

		long timestamp = Long.parseLong(filteredDate);
		newDate = new Date(timestamp);

		return newDate;

	}

	private String epochDateToString(String epochDate, String format) {

		String filteredDate = null;
		Date newDate = null;

		if(epochDate.contains("/Date")) {

			filteredDate = epochDate.replace("/Date(", "").replace(")/", "");

		} else {

			filteredDate = epochDate;
		}

		//"dd/MM/yyyy HH:mm:ss"
		SimpleDateFormat dateFormat = new java.text.SimpleDateFormat(format);

		long timestamp = Long.parseLong(filteredDate);
		newDate = new Date(timestamp);

		return dateFormat.format(newDate);

	}

	private String dateStringToDateEpoch(String string, String format) throws ParseException {

		SimpleDateFormat parser = new SimpleDateFormat(format);
		Date date = parser.parse(string);

		long epoch = date.getTime();

		return String.valueOf(epoch);

	}

	public String replaceArgument(String string, int index, String value) {

		List<Integer> found = new ArrayList<Integer>();
		String finalString = string;

		int indexFound = string.indexOf("?");

		while (indexFound >= 0) {
			found.add(indexFound);
			indexFound = string.indexOf("?", indexFound + 1);
		}

		if(found.size() >= index) {
			String first = string.substring(index);
			String second = string.substring(index + 1);

			finalString = first + value + second;
		}

		return finalString;
	}

	public void setUseStandardConnectionEngine(boolean useStandardConnectionEngine) {
		this.m_useStandardConnectionEngine = useStandardConnectionEngine;
	}

	public boolean isUseStandardConnectionEngine() {
		return m_useStandardConnectionEngine;
	}

	public void setVerbose(boolean verbose) {
		this.m_verbose = verbose;
	}

	public boolean isVerbose() {
		return m_verbose;
	}

	public void setProxyHost(String aHost)
	{
		if (aHost != null) {
			aHost = aHost.trim();
		}
		this.m_proxyHost = aHost;
	}

	public String getProxyHost()
	{
		return this.m_proxyHost;
	}

	public void setProxyPort(String aHost) {
		if (aHost != null) {
			aHost = aHost.trim();
		}
		this.m_proxyPort = aHost;
	}

	public String getProxyPort()
	{
		return this.m_proxyPort;
	}

	public void setProxyUser(String aUser)
	{
		if (aUser != null) {
			aUser = aUser.trim();
		}
		this.m_proxyUser = aUser;
	}

	public String getProxyUser()
	{
		return this.m_proxyUser;
	}

	public void setProxyPassword(String aPassword)
	{
		if (aPassword != null) {
			aPassword = aPassword.trim();
		}
		this.m_proxyPassword = aPassword;
	}

	public String getProxyPassword()
	{
		return this.m_proxyPassword;
	}


}
