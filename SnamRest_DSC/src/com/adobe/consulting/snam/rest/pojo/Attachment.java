package com.adobe.consulting.snam.rest.pojo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Attachment implements Serializable{
	
	@JsonProperty("__metadata")
	private AttachmentMetadata __metadata = new AttachmentMetadata();
	
	@JsonProperty("userId")
	private String userId;
	
	@JsonProperty("externalId")
	private String externalId;
	
	@JsonProperty("fileName")
	private String fileName;
	
	@JsonProperty("module")
	private String module;
	
	@JsonProperty("description")
	private String description;
	
	@JsonProperty("fileContent")
	private String fileContent;
	
	@JsonProperty("viewable")
	private Boolean viewable = true;
	
	@JsonProperty("deletable")
	private Boolean deletable = false;
	
	public Attachment() {
		__metadata.setUri("Attachment");
	}
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFileContent() {
		return fileContent;
	}

	public void setFileContent(String fileContent) {
		this.fileContent = fileContent;
	}

	public Boolean getViewable() {
		return viewable;
	}

	public void setViewable(Boolean viewable) {
		this.viewable = viewable;
	}

	public Boolean getDeletable() {
		return deletable;
	}

	public void setDeletable(Boolean deletable) {
		this.deletable = deletable;
	}
	
	public void setMetadata(AttachmentMetadata metadata) {
		this.__metadata = metadata;
	}
	
	public AttachmentMetadata getMetadata() {
		return __metadata;
	}
	
	
}
