package com.adobe.consulting.snam.rest.pojo;

import java.io.Serializable;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/odata/v2")
public interface SnamRestServiceProxy extends Serializable{

	@GET
	@Path("/EmpJob")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response EmpJob(
			@QueryParam("$format") String format, 
			@QueryParam("$filter") String filter, 
			@QueryParam("$select") String select,
			@QueryParam("$expand") String expand,
			@QueryParam("fromDate") String fromDate,
			@QueryParam("toDate") String toDate,
			@QueryParam("$skiptoken") String skiptoken);

	@GET
	@Path("/EmpEmployment")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response EmpEmployment(
			@QueryParam("$format") String format, 
			@QueryParam("$filter") String filter, 
			@QueryParam("$select") String select,
			@QueryParam("$expand") String expand,
			@QueryParam("fromDate") String fromDate,
			@QueryParam("toDate") String toDate,
			@QueryParam("$skiptoken") String skiptoken);

	@GET
	@Path("/PerPersonal")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response PerPersonal(
			@QueryParam("$format") String format, 
			@QueryParam("$filter") String filter, 
			@QueryParam("$select") String select,
			@QueryParam("$expand") String expand,
			@QueryParam("fromDate") String fromDate,
			@QueryParam("toDate") String toDate,
			@QueryParam("$skiptoken") String skiptoken);

	@GET
	@Path("/EmpCompensation")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response EmpCompensation(
			@QueryParam("$format") String format, 
			@QueryParam("$filter") String filter, 
			@QueryParam("$select") String select,
			@QueryParam("$expand") String expand,
			@QueryParam("fromDate") String fromDate,
			@QueryParam("toDate") String toDate,
			@QueryParam("$skiptoken") String skiptoken);

	@GET
	@Path("/EmpPayCompRecurring")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response EmpPayCompRecurring(
			@QueryParam("$format") String format, 
			@QueryParam("$filter") String filter, 
			@QueryParam("$select") String select,
			@QueryParam("$expand") String expand,
			@QueryParam("fromDate") String fromDate,
			@QueryParam("toDate") String toDate,
			@QueryParam("$skiptoken") String skiptoken);

	@GET
	@Path("/EmpPayCompNonRecurring")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response EmpPayCompNonRecurring(
			@QueryParam("$format") String format, 
			@QueryParam("$filter") String filter, 
			@QueryParam("$select") String select,
			@QueryParam("$expand") String expand,
			@QueryParam("fromDate") String fromDate,
			@QueryParam("toDate") String toDate,
			@QueryParam("$skiptoken") String skiptoken);

	@POST
	@Path("/upsert")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response UpsertPhase1(String jsonObj);

	@POST
	@Path("/upsert")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response UpsertPhase2(String jsonObj);

	@POST
	@Path("/upsert")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response UpsertPhase3(String jsonObj);

}

