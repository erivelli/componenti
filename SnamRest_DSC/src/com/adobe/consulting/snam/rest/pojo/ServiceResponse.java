package com.adobe.consulting.snam.rest.pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonTypeResolver;

public class ServiceResponse implements Serializable
{
	@JsonProperty("results")
	protected List<SFOData> records = new ArrayList<SFOData>();
	
	@JsonProperty("__next")
	protected String skipToken;
	
	protected final String SKIP_TOKEN = "$skiptoken=";
	
	protected boolean hasNext = false;

	public void setRecords(List<SFOData> records) {
		this.records = records;
	}
	public List<SFOData> getRecords() {
		return this.records;
	}

	public String getSkipToken() {
		return skipToken;
	}

	public void setSkipToken(String skipToken) {
		this.skipToken = skipToken;
		this.hasNext = true;
	}

	public String getIdNextToken() {
		if(this.skipToken != null) {
			return skipToken.substring(skipToken.indexOf(SKIP_TOKEN) + SKIP_TOKEN.length());
		}else {
			return null;
		}
	}
	
	public boolean isHasNext() {
		return hasNext;
	}
	
	public int getResultCount() {
		if(records == null) {
			return 0;
		}else {
			return records.size();
		}
	}
}
