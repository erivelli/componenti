package com.adobe.consulting.snam.rest.pojo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EmpEmployment implements SFOData, Serializable {

	@JsonProperty("__metadata")
	private GenericMetadata metadata;

	@JsonProperty("userId")
	private String userId;
	
	@JsonProperty("personIdExternal")
	private String personIdExternal;
	
	@JsonProperty("formalName")
	private String formalName;
	
	//NAVIGATIONS
	@JsonProperty("personNav")
	PerPerson personNav = null;
	
	//NAVIGATIONS
	@JsonProperty("compInfoNav")
	ServiceResponse compInfoNav = null;
	
	//NAVIGATIONS
	@JsonProperty("empPayCompRecurringNav")
	EmpPayCompRecurring empPayCompRecurringNav = null;
	
	//NAVIGATIONS
	@JsonProperty("empPayCompNonRecurringNav")
	ServiceResponse empPayCompNonRecurringNav = null;

	@Override
	public GenericMetadata getMetadata() {
		return this.metadata;
	}
	@Override
	public void setMetadata(GenericMetadata metadata) {
		this.metadata = metadata;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPersonIdExternal() {
		return personIdExternal;
	}
	public void setPersonIdExternal(String personIdExternal) {
		this.personIdExternal = personIdExternal;
	}
	public String getFormalName() {
		return formalName;
	}
	public void setFormalName(String formalName) {
		this.formalName = formalName;
	}
	public PerPerson getPersonNav() {
		return personNav;
	}
	public void setPersonNav(PerPerson personNav) {
		this.personNav = personNav;
	}
	public ServiceResponse getCompInfoNav() {
		return compInfoNav;
	}
	public void setCompInfoNav(ServiceResponse compInfoNav) {
		this.compInfoNav = compInfoNav;
	}
	public EmpPayCompRecurring getEmpPayCompRecurringNav() {
		return empPayCompRecurringNav;
	}
	public void setEmpPayCompRecurringNav(EmpPayCompRecurring empPayCompRecurringNav) {
		this.empPayCompRecurringNav = empPayCompRecurringNav;
	}
	public ServiceResponse getEmpPayCompNonRecurringNav() {
		return empPayCompNonRecurringNav;
	}
	public void setEmpPayCompNonRecurringNav(ServiceResponse empPayCompNonRecurringNav) {
		this.empPayCompNonRecurringNav = empPayCompNonRecurringNav;
	}
	
}
