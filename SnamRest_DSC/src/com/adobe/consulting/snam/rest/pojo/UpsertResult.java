package com.adobe.consulting.snam.rest.pojo;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UpsertResult implements Serializable {
	
	@JsonProperty("d")
	List<UpsertResponse> response;
	
	public void setResponse(List<UpsertResponse> response) {
		this.response = response;
	}
	
	public List<UpsertResponse> getResponse() {
		return response;
	}
	
}
