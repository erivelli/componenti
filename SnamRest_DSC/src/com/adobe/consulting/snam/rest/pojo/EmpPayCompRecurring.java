package com.adobe.consulting.snam.rest.pojo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EmpPayCompRecurring implements SFOData, Serializable {

	@JsonProperty("__metadata")
	private GenericMetadata metadata;

	@JsonProperty("userId")
	private String userId;
	
	@JsonProperty("personIdExternal")
	private String personIdExternal;
	
	@JsonProperty("formalName")
	private String formalName;
	
	@JsonProperty("payComponent")
	private String payComponent;

	@JsonProperty("frequency")
	private String frequency;

	@JsonProperty("currency")
	private String currency;
	
	@JsonProperty("currencyCode")
	private String currencyCode;
	
	@JsonProperty("paycompvalue")
	private String paycompvalue;
	
	@JsonProperty("customdouble2")
	private String customdouble2;

	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPayComponent() {
		return payComponent;
	}
	public void setPayComponent(String payComponent) {
		this.payComponent = payComponent;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getPayCompValue() {
		return paycompvalue;
	}
	public void setPayCompValue(String payCompValue) {
		this.paycompvalue = payCompValue;
	}
	@Override
	public GenericMetadata getMetadata() {
		return metadata;
	}
	@Override
	public void setMetadata(GenericMetadata metadata) {
		this.metadata = metadata;
	}
	public String getCustomdouble2() {
		return customdouble2;
	}
	public void setCustomdouble2(String customdouble2) {
		this.customdouble2 = customdouble2;
	}
	@Override
	public String getPersonIdExternal() {
		return personIdExternal;
	}
	@Override
	public void setPersonIdExternal(String personIdExternal) {
		this.personIdExternal = personIdExternal;
	}
	@Override
	public String getFormalName() {
		return formalName;
	}
	@Override
	public void setFormalName(String formalName) {
		this.formalName = formalName;
	}

}
