package com.adobe.consulting.snam.rest.pojo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CompStatementDetails implements Serializable{

	@JsonProperty("__metadata")
	private CompStatementDetailsMetadata __metadata;
	
	@JsonProperty("cust_Comp_Statement_Header_externalCode")
	private String cust_Comp_Statement_Header_externalCode;
	
	@JsonProperty("externalCode")
	private String externalCode;
	
	@JsonProperty("cust_Anno_Politica")
	private String cust_Anno_Politica;
	
	public CompStatementDetails() {
		
	}
			
	public CompStatementDetails(String baseUri, String cust_Comp_Statement_Header_externalCode, String externalCode, String cust_Anno_Politica) {
		
		baseUri = baseUri.replace("xxx", cust_Comp_Statement_Header_externalCode).replace("yyy", externalCode);
		
		__metadata = new CompStatementDetailsMetadata();
		
		__metadata.setUri(baseUri);
		
		this.cust_Comp_Statement_Header_externalCode = cust_Comp_Statement_Header_externalCode;
		this.externalCode = externalCode;
		this.cust_Anno_Politica = cust_Anno_Politica;
	}
	
	public void setMetadata(CompStatementDetailsMetadata metadata) {
		this.__metadata = metadata;
	}
	public CompStatementDetailsMetadata getMetadata() {
		return __metadata;
	}
	public String getCust_Comp_Statement_Header_externalCode() {
		return cust_Comp_Statement_Header_externalCode;
	}
	public void setCust_Comp_Statement_Header_externalCode(String cust_Comp_Statement_Header_externalCode) {
		this.cust_Comp_Statement_Header_externalCode = cust_Comp_Statement_Header_externalCode;
	}
	public String getExternalCode() {
		return externalCode;
	}
	public void setExternalCode(String externalCode) {
		this.externalCode = externalCode;
	}
	public String getCust_Anno_Politica() {
		return cust_Anno_Politica;
	}
	public void setCust_Anno_Politica(String cust_Anno_Politica) {
		this.cust_Anno_Politica = cust_Anno_Politica;
	}
}
