package com.adobe.consulting.snam.rest.pojo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EmpPayCompNonRecurring implements SFOData, Serializable {

	@JsonProperty("__metadata")
	private GenericMetadata metadata;

	@JsonProperty("userId")
	private String userId;
	
	@JsonProperty("personIdExternal")
	private String personIdExternal;
	
	@JsonProperty("formalName")
	private String formalName;
	
	@JsonProperty("value")
	private String value;
	
	@JsonProperty("payDate")
	private String payDate;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getPayDate() {
		return payDate;
	}
	public void setPayDate(String payDate) {
		this.payDate = payDate;
	}
	@Override
	public GenericMetadata getMetadata() {
		return metadata;
	}
	@Override
	public void setMetadata(GenericMetadata metadata) {
		this.metadata = metadata;
	}
	@Override
	public String getPersonIdExternal() {
		return personIdExternal;
	}
	@Override
	public void setPersonIdExternal(String personIdExternal) {
		this.personIdExternal = personIdExternal;
	}
	@Override
	public String getFormalName() {
		return formalName;
	}
	@Override
	public void setFormalName(String formalName) {
		this.formalName = formalName;
	}

}
