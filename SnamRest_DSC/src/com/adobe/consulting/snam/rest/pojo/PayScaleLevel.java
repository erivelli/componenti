package com.adobe.consulting.snam.rest.pojo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PayScaleLevel implements SFOData, Serializable {

	@JsonProperty("__metadata")
	private GenericMetadata metadata;
	
	@JsonProperty("payScaleLevel")
	private String payScaleLevel;
	
	@JsonProperty("externalName_it_IT")
	private String externalName_it_IT;
	
	public String getPayScaleLevel() {
		return payScaleLevel;
	}
	
	public void setPayScaleLevel(String payScaleLevel) {
		this.payScaleLevel = payScaleLevel;
	}
	
	public String getExternalName_it_IT() {
		return externalName_it_IT;
	}
	
	public void setExternalName_it_IT(String externalName_it_IT) {
		this.externalName_it_IT = externalName_it_IT;
	}
	
	@Override
	public GenericMetadata getMetadata() {
		return this.metadata;
	}
	@Override
	public void setMetadata(GenericMetadata metadata) {
		this.metadata = metadata;
	}
	@Override
	public String getUserId() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void setUserId(String userId) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public String getPersonIdExternal() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void setPersonIdExternal(String personIdExternal) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public String getFormalName() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void setFormalName(String formalName) {
		// TODO Auto-generated method stub
		
	}
}
