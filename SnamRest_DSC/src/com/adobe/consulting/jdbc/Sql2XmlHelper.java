package com.adobe.consulting.jdbc;

import com.adobe.consulting.snam.rest.bean.XmlDocumentInfoBean;
import com.adobe.idp.dsc.util.DOMUtil;
import com.adobe.idp.dsc.util.TextUtil;
import com.adobe.workflow.pat.service.PATExecutionContext;

import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Attr;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import sun.misc.BASE64Encoder;

public class Sql2XmlHelper
{
	public static int TEST_NUMBER_ROWS = 5;
	private PATExecutionContext m_patExecContext;

	public Sql2XmlHelper() {}

	public Sql2XmlHelper(PATExecutionContext patExecContext)
	{
		this.m_patExecContext = patExecContext;
	}

	public Document writeXmlDocument(ResultSet resultSet, XmlDocumentInfoBean xmlInfo) throws ParserConfigurationException, SQLException {
		Document document = null;
		document = writeXmlDocumentElementStyle(resultSet, xmlInfo);
		return document;
	}

	public Document writeXmlDocument(ResultSet resultSet) throws ParserConfigurationException, SQLException {
		Document document = null;
		document = writeXmlDocumentElementStyle(resultSet);
		return document;
	}

	private Document writeXmlDocumentElementStyle(ResultSet resultSet) throws ParserConfigurationException, SQLException
	{
		Document document = DOMUtil.newDocument();

		Element rootElement = document.createElement("root");

		BASE64Encoder encoder = new BASE64Encoder();

		document.appendChild(rootElement);
		if (resultSet != null)
		{
			ResultSetMetaData resultSetMetaData = resultSet.getMetaData();

			while (resultSet.next()) {

				Element repeatingElement = DOMUtil.createElement(rootElement, "row");

				for (int i = 1; i <= resultSetMetaData.getColumnCount() ; i++) {

					Object object = resultSet.getObject(i);
					String columnName = resultSetMetaData.getColumnName(i);
					String columnTypeName = resultSetMetaData.getColumnTypeName(i);
					String objectString = object == null ? "null" : object.toString();

					if (objectString != null) {
						Element element = null;
						if (objectString.contains("&") || objectString.contains("<") || objectString.contains(">")) {
							element = DOMUtil.createElement(repeatingElement, columnName);
							element.appendChild(document.createCDATASection(objectString));
						}
						else {

							if(columnTypeName.toLowerCase().contains("blob") || 
							   columnTypeName.toLowerCase().contains("varbinary") ||
							   columnTypeName.toLowerCase().contains("image")){
								Blob imgAsBlob = resultSet.getBlob(i);

								if(imgAsBlob != null) {
									byte[] imgAsBytes = imgAsBlob.getBytes(1, (int) imgAsBlob.length());

									//BASE64
									String encoded = encoder.encode(imgAsBytes);

									element = DOMUtil.createElementWithText(repeatingElement, columnName, encoded);
								}else {
									element = DOMUtil.createElementWithText(repeatingElement, columnName, "null");
								}
							}else{
								element = DOMUtil.createElementWithText(repeatingElement, columnName, objectString);
							}

						}

						if (element != null) {
							DOMUtil.createAttribute(element, "type").setValue(columnTypeName);
						}
					}
				}
			}
		}
		return document;
	}

	private Document writeXmlDocumentElementStyle(ResultSet resultSet, XmlDocumentInfoBean xmlInfo) throws ParserConfigurationException, SQLException
	{
		Document document = DOMUtil.newDocument();
		String rootName = xmlInfo.getRootName();

		if (!TextUtil.isEmpty(rootName))
			rootName = this.m_patExecContext.replacePathExpressions(rootName);
		if (TextUtil.isEmpty(rootName)) {
			rootName = "root";
		}
		String repeatingElementName = xmlInfo.getRepeatingElementName();
		if (!TextUtil.isEmpty(repeatingElementName))
			repeatingElementName = this.m_patExecContext.replacePathExpressions(repeatingElementName);
		if (TextUtil.isEmpty(repeatingElementName)) {
			repeatingElementName = "element";
		}

		boolean escapeIllegalCharacters = xmlInfo.isEscapeIllegalCharacters();


		Element rootElement = document.createElement(rootName);
		List columnNameMappings = xmlInfo.getColumnNameMappings();

		document.appendChild(rootElement);
		if (resultSet != null)
		{
			ResultSetMetaData resultSetMetaData = resultSet.getMetaData();

			while (resultSet.next()) {
				Element repeatingElement = DOMUtil.createElement(rootElement, repeatingElementName);
				if (columnNameMappings != null) {
					int mappingsSize = columnNameMappings.size();
					for (int i = 0; i < mappingsSize; i++) {
						Map map = (Map)columnNameMappings.get(i);
						String columnIndexString = (String)map.get("column_index");
						String columnName = (String)map.get("column_name");
						String userDefinedElementName = (String)map.get("element_name");
						if (!TextUtil.isEmpty(userDefinedElementName))
							columnName = userDefinedElementName;
						Integer integer = new Integer(columnIndexString);
						int columnIndex = integer.intValue();
						Object object = resultSet.getObject(columnIndex);
						String columnTypeName = resultSetMetaData.getColumnTypeName(columnIndex);
						String objectString = object == null ? "null" : object.toString();


						if (objectString != null) {
							Element element = null;
							if ((!escapeIllegalCharacters) && ((objectString.contains("&")) || (objectString.contains("<")) || (objectString.contains(">")))) {
								element = DOMUtil.createElement(repeatingElement, columnName);
								element.appendChild(document.createCDATASection(objectString));
							}
							else {
								element = DOMUtil.createElementWithText(repeatingElement, columnName, objectString);
							}

							if (element != null) {
								DOMUtil.createAttribute(element, "type").setValue(columnTypeName);
							}
						}
					}
				}
			}
		}
		return document;
	}

	private Document writeTestXmlDocumentElementStyle(ResultSet resultSet, XmlDocumentInfoBean xmlInfo) throws ParserConfigurationException, SQLException, DOMException
	{
		Document document = DOMUtil.newDocument();
		String rootName = xmlInfo.getRootName();
		if (TextUtil.isEmpty(rootName))
			rootName = "root";
		String repeatingElementName = xmlInfo.getRepeatingElementName();
		if (TextUtil.isEmpty(repeatingElementName))
			repeatingElementName = "element";
		Element rootElement = document.createElement(rootName);
		List columnNameMappings = xmlInfo.getColumnNameMappings();

		boolean escapeIllegalCharacters = xmlInfo.isEscapeIllegalCharacters();


		document.appendChild(rootElement);
		if (resultSet != null)
		{
			ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
			int index = 0;
			while ((resultSet.next()) && (index < TEST_NUMBER_ROWS)) {
				index++;
				Element repeatingElement = DOMUtil.createElement(rootElement, repeatingElementName);
				if (columnNameMappings != null) {
					int mappingsSize = columnNameMappings.size();
					for (int i = 0; i < mappingsSize; i++) {
						Map map = (Map)columnNameMappings.get(i);
						String columnIndexString = (String)map.get("column_index");
						String columnName = (String)map.get("column_name");
						String userDefinedElementName = (String)map.get("element_name");
						if (!TextUtil.isEmpty(userDefinedElementName))
							columnName = userDefinedElementName;
						Integer integer = new Integer(columnIndexString);
						int columnIndex = integer.intValue();
						Object object = resultSet.getObject(columnIndex);
						String columnTypeName = resultSetMetaData.getColumnTypeName(columnIndex);
						String objectString = object == null ? "null" : object.toString();


						if (objectString != null) {
							Element element = null;
							if ((!escapeIllegalCharacters) && ((objectString.contains("&")) || (objectString.contains("<")) || (objectString.contains(">")))) {
								element = DOMUtil.createElement(repeatingElement, columnName);
								element.appendChild(document.createCDATASection(objectString));
							}
							else {
								element = DOMUtil.createElementWithText(repeatingElement, columnName, objectString);
							}
						}
					}
				}
			}
		}
		return document;
	}
}

